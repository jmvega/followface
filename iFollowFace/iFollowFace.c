/*
 *  Copyright (C) 2009 Julio M. Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio M. Vega Pérez <julio.vega@urjc.es>
 */

#include "iFollowFace.h"

int iFollowFace_id=0;
int iFollowFace_brothers[MAX_SCHEMAS];
arbitration iFollowFace_callforarbitration;

enum iFollowFace_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int iFollowFace_state;

static CvMemStorage* storage = 0;
static CvMemStorage* storageTmp = 0;
static CvMemStorage* storageAux = 0;
static CvHaarClassifierCascade* cascade = 0;
const char* cascade_name = "haarcascade_frontalface_alt.xml";
IplImage *imgLocal = NULL;
CvMat img; // Envoltura a mycolorA
char *image;
char *imgLocalRGB;
int display_active = 0;
CvSeq *facesTmp, *facesAux;
double detectionTimeAux;
float absoluteDiffX, absoluteDiffY;

/* GTK variables */
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkWidget *canvas;

/* images X server variables */
static XImage *imagenA, *imagenFiltrada;
static char *imagenA_buf, *imagenFiltrada_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

/* imported variables*/
int mysoncolorA;
unsigned char **mycolorA=NULL;
float *myencoders=NULL;
runFn mycolorAresume;
stopFn mycolorAsuspend;
runFn colorArun;
stopFn colorAstop;
runFn encodersrun, motorsrun;
stopFn encodersstop, motorsstop;
int mycolorAheight;
int mycolorAwidth;
int mycolorAnChannels;

/* exported variables */
int iFollowFace_cycle=100; /* ms */
double detectionTime;
CvSeq *faces;
pthread_mutex_t iFollowFaceMutex;
unsigned long int facesClock;
CvPoint center;
double systemInstant;
double timeForced;
double timeToForcedSearch;
double startToFollowTime, actualFollowTime;
int completedSearch;
int completedTrack;
int isForcedSearch;
int myActualState;

float angulo_x, angulo_y;
int last_movement; // Indica el ultimo movimiento que ha realizado la pantilt, en "local"
int last_full_movement; // Indica el ultimo movimiento que ha realizado la pantilt, en "global"
struct dfilter data_filter;
struct faceStruct* myAllFaces; // vamos a suponer que como mucho tendremos 2 caretos en la escena
int facesStructCounter;
int randomPosition;
int nextLatitude;
int nextLongitude;

runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;
static SofReference mypioneer;

inline float distancia (imagePoint punto1, imagePoint punto2) {
  return (sqrt (pow((punto2.x-punto1.x), 2.)+pow((punto2.y-punto1.y), 2.)));
}

inline double pasar_radianes_grados (double radianes) {
  return ((radianes * 180.)/PI);
}

inline double pasar_grados_radianes (double grados) {
  return ((grados * PI)/180.);
}

inline scenePoint coordenadas2PT (int x, int y) {
	imagePoint punto1, punto2, punto3, punto_origen;
	t_vector vector1, vector2;
	float dist, ang1, ang2;
	scenePoint c_angulares;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	punto3.x = x; punto3.y = y;
	punto_origen.x = CENTRO_X; punto_origen.y = CENTRO_Y;
	dist =  distancia (punto3, punto_origen);

	c_angulares.tilt = (((dist - punto1.y)*(vector1.x))/(vector1.y))+punto1.x;
	  
	vector1.x = CENTRO_X - x;
	vector1.y = CENTRO_Y - y;
	vector2.x = CENTRO_X - ANCHO_ESCENA_COMPUESTA / 2;
	vector2.y = CENTRO_Y - 0;
	ang1 = atan2 (vector1.x, vector1.y);
	ang2 = atan2 (vector2.x, vector2.y);

	c_angulares.pan = -1*(pasar_radianes_grados (ang2-ang1));

	return c_angulares;
}

inline imagePoint PT2coordenadas (scenePoint PT) {
	imagePoint_float punto1, punto2;
	t_vector_float vector1;
	float radio;
	imagePoint coordenada;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	radio = (vector1.y/vector1.x)*(PT.tilt-punto1.x)+punto1.y;

	coordenada.x = (sin (pasar_grados_radianes(-1*PT.pan)) * radio) + CENTRO_X;
	coordenada.y = CENTRO_Y - (cos (pasar_grados_radianes(-1*PT.pan)) * radio);

	if ((coordenada.x < 0) ||
		(coordenada.x > ANCHO_ESCENA_COMPUESTA) ||
		(coordenada.y < 0) ||
		(coordenada.y > ALTO_ESCENA_COMPUESTA)) {
			coordenada.x = -1;
			coordenada.y = -1;
	}

	return coordenada;
}

inline void drawCross (char *image, int x, int y, int side, unsigned char r, unsigned char g, unsigned char b) {
		int i, offset;

		// linea vertical
		for (i=-side; i<side+1; i++) {
			offset = 320 * (y+i) * 3 + (x * 3);
			if (offset > 0) {
			image[offset + 0] = b;
			image[offset + 1] = g;
			image[offset + 2] = r;
			}
		}

		// linea horizontal
		for (i=-side; i<side+1; i++) {
			offset = 320 * y * 3 + ((x+i) * 3);
			if (offset > 0) {
			image[offset + 0] = b;
			image[offset + 1] = g;
			image[offset + 2] = r;
			}
		}
}

inline void detectaCaras(IplImage *img) {
  static unsigned long int lastClock = 0;
  cvClearMemStorage( storageTmp );
	int i;

  if( cascade && img )
  {
    //detectionTimeAux = (double) cvGetTickCount();
    facesTmp = cvHaarDetectObjects( img, cascade, storageTmp,
                                  1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                  cvSize(MIN_EXP_WIDTH, MIN_EXP_HEIGHT) );
		//--------------------------------------------------------------------------
		// A little of general culture :)
		// cvGetTickCount(void): Returns the number of ticks.
		// cvGetTickFrequency(void): Returns the number of ticks per microsecond.
		// Thus, the quotient of cvGetTickCount() and cvGetTickFrequency() will give
		// the number of microseconds starting from the platform-dependent event.
		//--------------------------------------------------------------------------

    //detectionTimeAux = (double) cvGetTickCount() - detectionTimeAux; // ticks to detect face...
    //detectionTimeAux =  detectionTimeAux/((double)cvGetTickFrequency()*1000.); // ...translated into seconds

    //pthread_mutex_lock(&iFollowFaceMutex);
    //facesClock = lastClock;
    //lastClock++;
    //pthread_mutex_unlock(&iFollowFaceMutex);
  }
}

inline void dibujaCaras(IplImage* dst) {
   int scale = 1;
   CvRect r;
   CvPoint pt1, pt2;
   int i;

	if (facesTmp->total == 0) {
		center.x = 0;
		center.y = 0;
	} else {
		for( i = 0; i < (facesTmp ? facesTmp->total : 0); i++ ) {
      r = *(CvRect*)cvGetSeqElem(facesTmp, i);
      center.x = cvRound((r.x + r.width*0.5));
      center.y = cvRound((r.y + r.height*0.5));
		}
	}
}

inline void trackFace () {
  /* hallamos las coordenadas reales para el centro (160,120)   */
  data_filter.x = center.x - 160;
  data_filter.y = center.y - 120;

  /* miramos en que cuadrante se encuentra */
  if (data_filter.x > 0)
    if (data_filter.y > 0)
      data_filter.cuadrante=1;
    else
      data_filter.cuadrante=3;
  else
    if (data_filter.y > 0)
      data_filter.cuadrante=2;
    else
	    data_filter.cuadrante=4;

  if ((data_filter.x==0) && (data_filter.y==0))
    data_filter.cuadrante=0;

	angulo_x = ENCOD_TO_DEG * ((int) abs(data_filter.x) );
	angulo_y = ENCOD_TO_DEG * ((int) abs(data_filter.y) );

	*mylongitude_speed = (VEL_MAX_PAN-((POS_MAX_PAN-(abs(data_filter.x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN))));
	*mylatitude_speed = (VEL_MAX_TILT-((POS_MAX_TILT-(abs(data_filter.y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT))));

	switch (data_filter.cuadrante) {
		case 1:
			*mylatitude = -angulo_y + (*mytilt_angle);
			*mylongitude = -angulo_x + (*mypan_angle);
		  break;
		case 2:
			*mylatitude = -angulo_y + (*mytilt_angle);
			*mylongitude = angulo_x + (*mypan_angle);
		  break;
		case 3:
			*mylatitude = angulo_y + (*mytilt_angle);
			*mylongitude = -angulo_x + (*mypan_angle);
		  break;
		case 4:
			*mylatitude = angulo_y + (*mytilt_angle);
			*mylongitude = angulo_x + (*mypan_angle);
		  break;
	}

//	if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
		completedTrack = TRUE;
//	}	else {
//		completedTrack = FALSE;
//	}
}

inline void RGB2GRAY (CvMat *img, IplImage *imgOut) {
	int i, j, c, row, k;
	double media;
	CvScalar s;

	for(i=0; i<240*320; i++) {
		c=i%320;
		row=i/320;
		j=row*320+c;

		s=cvGet2D(img,row,c); // obtenemos el valor en el píxel (i,j)

		// Very interesting information :)
		// To convert any color to its most approximate level of gray, first one must obtain the values of its red, green and blue (RGB) primaries.
		// Then it is sufficient to add 30% of the red value plus 59% of that of the green plus 11% of that of the blue, no matter whatever scale is employed.
    // The resultant level is the desired gray value. These percentages are chosen due to the different relative sensitivity of the normal human eye to each 
		// of the primary colors (higher to the green, lower to the blue).

		media=(s.val[0]+s.val[1]+s.val[2])/3;
		s.val[0]=media;
		s.val[1]=media;
		s.val[2]=media;
		cvSet2D(imgOut,row,c,s);

		imgLocalRGB [i*3+0] = media;
		imgLocalRGB [i*3+1] = media;
		imgLocalRGB [i*3+2] = media;
	}
}

inline void generateVirtualFace (double actualInstant) {
	struct faceStruct *myActualFace, *p, *q, *r;
	scenePoint myScenePoint;
	myActualFace = NULL;
	int signoP, signoT;

	myActualFace = (struct faceStruct*) malloc (sizeof (struct faceStruct));

	myActualFace->firstInstant = actualInstant; // instante de detección, en segundos
	myActualFace->lastInstant = myActualFace->firstInstant;

	if (randomPosition == TRUE) {
	  srand(time(0));
		signoP = random()%2;
		signoT = random()%2;
		if (signoP==1)
			myActualFace->longitude = -random()%159;
		else
			myActualFace->longitude = random()%159;

		if (signoT==1)
			myActualFace->latitude = -random()%31;
		else
			myActualFace->latitude = random()%31;

		randomPosition = FALSE;
	} else { // toca posicionarse en la siguiente hubicación sistemática
		myActualFace->longitude = longitudeScenePositions[nextLongitude];
		myActualFace->latitude = latitudeScenePositions[nextLatitude];

		if (nextLongitude == 7) {
			nextLongitude = 0;
			if (nextLatitude == 3)
				nextLatitude = 0;
			else nextLatitude ++;
		} else nextLongitude ++;

		randomPosition = TRUE;
	}

	myActualFace->saliency = MAX_SALIENCY;
	myActualFace->liveliness = MIN_LIVELINESS;
	myActualFace->pixel_x = 0;
	myActualFace->pixel_y = 0;
	myScenePoint = coordenadas2PT (myActualFace->pixel_x, myActualFace->pixel_y); // obtenemos el punto en escena
	myActualFace->scenePoint_pan = myScenePoint.pan;
	myActualFace->scenePoint_tilt = myScenePoint.tilt;
	myActualFace->type = 1; // cara virtual
	myActualFace->isVisited = FALSE;

	printf ("Virtual face generated on = [%.2f, %.2f]\n", myActualFace->longitude, myActualFace->latitude);

	p = myAllFaces;
	if (facesStructCounter != 0) { // ahora haremos uso de p
		if (p->type == 1) { // es una cara virtual así que la eliminamos
			printf ("FORCED: Virtual face analized and without face --> Deleting...\n");
			myAllFaces = myAllFaces->next;
			q = myAllFaces;
			free (p);
			p = q;
			facesStructCounter --;
		} else {
			r = p;
			while (r->next != NULL) { // Llegamos hasta el último de la cola
				r = r->next;
				r->lastInstant = actualInstant;
				printf ("Bucle búsqueda\n");
			}

			p->saliency = MIN_SALIENCY; // pasará a ser la última así que saliencia mínima
			p->lastInstant = actualInstant;

			myAllFaces = myAllFaces->next; // también podría ser p->next, ya que es lo mismo :)
			r->next = p;
			p->next = NULL;

			if (myAllFaces != NULL)
				p = myAllFaces;
		}
	}
	myAllFaces = myActualFace;
	myActualFace->next = p;

	facesStructCounter ++;
}

inline void analizeImage (double actualInstant) {
	static unsigned char **mycolorATmp = NULL;
	struct faceStruct *p;

	if (mycolorA != mycolorATmp) { // Source image has been changed
		int type;
		switch (mycolorAnChannels) {
			case 1: type = CV_8UC1;
			break;
			case 3: type = CV_8UC3;
			break;
			case 4: type = CV_8UC4;
			break;
			default: fprintf(stderr, "iFollowFace: Incorrect number of channels for input image\n");
			jdeshutdown(1);
		}
		img = cvMat(240, 320, type, *mycolorA);

		if (imgLocal != NULL)
			cvReleaseImage(&imgLocal);

		imgLocal = cvCreateImage(cvSize(mycolorAwidth, mycolorAheight), IPL_DEPTH_8U, 1);
		mycolorATmp = mycolorA;
	}

	if (imgLocal != NULL) {
		switch (mycolorAnChannels) {
			case 1: cvCopy(&img, imgLocal, NULL);
			break;
			case 3: RGB2GRAY (&img, imgLocal); // son excluyentes ambas funciones, hacen lo mismo, o usamos una u otra :)
			//cvCvtColor(&img, imgLocal, CV_BGR2GRAY);
			break;
			case 4: cvCvtColor(&img, imgLocal, CV_BGRA2GRAY);
			break;
		}

		detectaCaras(imgLocal);
		dibujaCaras(imgLocal);
	}

	printf ("facesTmp actual = %i\n", facesTmp->total);
	if (facesTmp->total > 0) { // hemos detectado caras
		if (myAllFaces->type == 1) { // se trataba de una posición virtual, convertimos a real, proceso de creación de cara real
			printf ("Virtual Face --> Real Face\n");
			myAllFaces->type = 0;
			myAllFaces->lastInstant = actualInstant;
			myAllFaces->liveliness = MAX_LIVELINESS;
		}
		else myAllFaces->liveliness += LIVELINESS_INCREMENT*BONUS_FACTOR; // damos un positivo porque hay cara :)
		if (myAllFaces->liveliness > MAX_LIVELINESS) // para evitar saturación
			myAllFaces->liveliness = MAX_LIVELINESS;

	} else if (myAllFaces->type == 0) { // se trataba de una posición con cara real, y ahora deja de haber cara -> otorgamos tiempo de confianza
		myAllFaces->liveliness -= LIVELINESS_DECREMENT*PENALTY_FACTOR;
	}

	if (myAllFaces != NULL)
		myAllFaces->isVisited = TRUE; // en cualquier caso, es una zona visitada
}

inline void searchObjetive () {
	float aux_x, aux_y, speed_x, speed_y;

	if (!completedSearch) {
		aux_y = myAllFaces->latitude; // + (*mytilt_angle);
		aux_x = myAllFaces->longitude; // + (*mypan_angle);

	  speed_x = 8*(VEL_MAX_PAN-((POS_MAX_PAN-DEG_TO_ENCOD*(abs(aux_x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN))));
	  speed_y = 8*(VEL_MAX_TILT-((POS_MAX_TILT-DEG_TO_ENCOD*(abs(aux_y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT))));

		if ((*mylongitude_speed) < VEL_MIN_PAN)
			*mylongitude_speed = VEL_MIN_PAN*4;
		else *mylongitude_speed = speed_x;
		if ((*mylatitude_speed) < VEL_MIN_TILT)
			*mylatitude_speed = VEL_MIN_TILT*4;
		else *mylatitude_speed = speed_y;

		if (aux_x > MAX_PAN_ANGLE)
			*mylongitude = MAX_PAN_ANGLE;
		else if (aux_x < MIN_PAN_ANGLE)
			*mylongitude = MIN_PAN_ANGLE;
		else
			*mylongitude = aux_x;

		if (aux_y > MAX_TILT_ANGLE)
			*mylatitude = MAX_TILT_ANGLE;
		else if (aux_y < MIN_TILT_ANGLE)
			*mylatitude = MIN_TILT_ANGLE;
		else
			*mylatitude = aux_y;

		if (((*mypan_angle) < ((*mylongitude) + 1.)) && ((*mypan_angle) > ((*mylongitude) - 1.)) && ((*mytilt_angle) < ((*mylatitude) + 1.)) && ((*mytilt_angle) > ((*mylatitude) - 1.))) {
			completedSearch = TRUE;
		}	else {
			completedSearch = FALSE;
		}
	}
}

inline void thinkObjetive () {
	//printf ("Trata de arrancarlo por dios!\n");
}

inline void updateFaces (double instant) {
	struct faceStruct *p, *q, *r;
	int found = FALSE;

	p = myAllFaces;

	// 1º - Recorremos la lista eliminando todos los que se les ha acabado la vida :(
	q = p;
	while (p != NULL) {
		printf ("Recorrido de caras (%i). Pos: [%.2f, %.2f], live = %.1f, sal = %.1f\n", facesStructCounter, p->longitude, p->latitude, p->liveliness, p->saliency);
		if ((p != myAllFaces) && (instant - p->lastInstant > TIME_TO_DEAD)) { // esta antigua cara será borrada de memoria
			// borrado en otro sitio que no sea el principio
			printf ("Deleting old face of memory...\n");
			q->next = p->next;
			free (p);
			p = q->next;
			facesStructCounter --;
		} else {
			p->liveliness -= LIVELINESS_DECREMENT;
			p->saliency += SALIENCY_INCREMENT;
			q = p;
			p = p->next; // preparamos a p para la siguiente vuelta :)
		}
	}

	p = myAllFaces;
  if (p != NULL) {
		if ((p->type == 0) && ((instant - p->lastInstant) > FOLLOW_TIME)) { // si es una cara real y se le ha pasado el tiempo de seguimiento
			printf ("FOLLOW TIME expired\n");

			r = p;
			while (r->next != NULL) // Llegamos hasta el último de la cola
				r = r->next;

			p->latitude = *mytilt_angle; // nos quedamos con la ultima posición trackeada de esta cara, para su revisita posterior...
			p->longitude = *mypan_angle;
			p->lastInstant = instant;

			if (myAllFaces->next != NULL) {
				myAllFaces = myAllFaces->next; // también podría ser p->next, ya que es lo mismo :)
				myAllFaces->lastInstant = instant;
				r->next = p;
				p->next = NULL; // será el último de la cola
			} // si myAllFaces->next == NULL, se queda apuntando al mismo que tiene, que es una cara real
		} else if ((p->type == 1) && (p->isVisited)) { // es una cara virtual ya analizada y que no contenía ninguna cara, la borramos
			printf ("Virtual face analized and without face --> Deleting...\n");
			myAllFaces = myAllFaces->next;
			q = myAllFaces;
			free (p);
			p = q;
			if (myAllFaces != NULL)
				myAllFaces->lastInstant = instant;
			facesStructCounter --;
		} else if ((p->type == 0) && (p->liveliness <= LIVELINESS_TO_DEAD)) { // se ha muerto, es posible, ya que si se deja de ver... nos lo cargamos :P
			printf ("This face has dead because of its liveliness\n");
			myAllFaces = myAllFaces->next;
			q = myAllFaces;
			free (p);
			p = q;
			if (myAllFaces != NULL)
				myAllFaces->lastInstant = instant;
			facesStructCounter --;
		} else if (p->type == 0) { // si es una cara real, que no le ha pasado el tiempo, vamos actualizando su vida
			p->saliency -= SALIENCY_DECREMENT;
			p->liveliness += LIVELINESS_INCREMENT;
			if (p->liveliness > MAX_LIVELINESS) // para evitar saturación
				p->liveliness = MAX_LIVELINESS;

//			p->latitude = *mytilt_angle; // vamos actualizando su posición, que puede haber variado...
//			p->longitude = *mypan_angle;
		}

		if (myAllFaces != NULL)
			printf ("Number of faces = %i, CARA ACTUAL: [%.2f, %.2f], live = %.1f, sal = %.1f\n", facesStructCounter, myAllFaces->longitude, myAllFaces->latitude, myAllFaces->liveliness, myAllFaces->saliency);
	}
}

void iFollowFace_iteration(){
	static char d = 0;
	int i;
	speedcounter(iFollowFace_id);
	double actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	struct faceStruct *actualFace, *p, *newFace;

	if (d == 0) {
		cascade = (CvHaarClassifierCascade*) cvLoad (cascade_name, 0, 0, 0 );
		if (!cascade) {
			fprintf(stderr, "iFollowFace: Could not load classifier cascade\n");
			jdeshutdown(1);
		}
		storage = cvCreateMemStorage(0);
		storageTmp = cvCreateMemStorage(0);
	}

	if (mycolorA != NULL) {
		updateFaces (actualInstant); // en función del tiempo, actualizaremos: vida, saliencia y quitar muertos
		timeToForcedSearch = facesStructCounter * TIME_TO_FORCED_SEARCH;

		if ((myActualState == think) && ((facesStructCounter == 0) || ((myAllFaces != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)))) {
			if ((myAllFaces != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)) {
				isForcedSearch = TRUE; // para que a la siguiente vuelta tengamos el flag activado y hagamos búsqueda
				timeForced = actualInstant;
				printf ("FORCED SEARCH\n");				
			}
			generateVirtualFace (actualInstant);
		}
		/* Aclaración de esta peazo de condición... Espero que el lector me lo agradezca :) **
		STATEMENT = (((myActualState == think) && (A)) || (isForcedSearch == TRUE))
		A = (facesStructCounter > 0) && ((facesTmp == NULL) || ((facesTmp <= 0) && (B)))
		B = (myAllFaces->type == 1) || ((myAllFaces->type == 0) && ((myAllFaces->liveliness) <= LIVELINESS_TO_DEAD))
		*/
		else if (((myActualState == think) && ((facesStructCounter > 0) && ((facesTmp == NULL) || ((facesTmp->total <= 0) && (((myAllFaces != NULL) && (myAllFaces->type == 1)) || (((myAllFaces != NULL) && (myAllFaces->type == 0)) && ((myAllFaces->liveliness) <= LIVELINESS_TO_DEAD))))))) || (isForcedSearch == TRUE)) {
			myActualState = search;
			isForcedSearch = FALSE;
		}

		else if ((myActualState == think) && (facesStructCounter > 0) && ((facesTmp->total > 0) || ((facesTmp-> total <= 0) && (myAllFaces->liveliness > LIVELINESS_TO_DEAD))))
			myActualState = track;

		else if ((myActualState == search) && (completedSearch)) {
			myActualState = analizeSearch;
			completedSearch = FALSE;
		}

		else if (myActualState == analizeSearch)
			myActualState = think;

		else if ((myActualState == track) && (completedSearch)) {
			myActualState = analizeTrack;
			completedSearch = FALSE;
		}

		else if (myActualState == analizeTrack)
			myActualState = think;

		switch (myActualState) {
			case think: printf ("state think\n"); thinkObjetive ();
			break;
			case search: printf ("state search\n"); searchObjetive ();
			break;
			case analizeSearch: printf ("state analizeSearch\n"); analizeImage (actualInstant);
			break;
			case track: printf ("state track\n"); searchObjetive ();
			break;
			case analizeTrack: printf ("state analizeTrack\n"); analizeImage (actualInstant);
			break;
		}
	} else // fin if imgLocal != NULL
		pthread_mutex_unlock(&iFollowFaceMutex);

	pthread_mutex_lock(&iFollowFaceMutex);
	detectionTime = detectionTimeAux;
	CV_SWAP(faces, facesTmp, facesAux);
	CV_SWAP(storage, storageTmp, storageAux);
	pthread_mutex_unlock(&iFollowFaceMutex);
	d++;
}

/*Importar símbolos*/
void iFollowFace_imports(){
	void *aux = NULL;
	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");

	motorsrun=(runFn)myimport("motors","run");
	motorsstop=(stopFn)myimport("motors","stop");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	printf ("ptmotors = %f, %f, %f, %f\n", *max_pan, *max_tilt, *min_pan, *min_tilt);

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");

	mycolorA = (unsigned char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

  mycolorAheight = 240;
  mycolorAwidth = 320;
  mycolorAnChannels = 3;
}

/*Exportar símbolos*/
void iFollowFace_exports(){
  myexport("iFollowFace","id",&iFollowFace_id);
  myexport("iFollowFace","cycle",&iFollowFace_cycle);
  myexport("iFollowFace","run",(void *)iFollowFace_run);
  myexport("iFollowFace","stop",(void *)iFollowFace_stop);
  myexport("iFollowFace", "detectionTime", &detectionTime);
  myexport("iFollowFace", "faces", &faces);
  myexport("iFollowFace", "iFollowFaceMutex", &iFollowFaceMutex);
  myexport("iFollowFace", "facesClock", &facesClock);
  myexport("iFollowFace", "center", &center);
}

void iFollowFace_stop()
{
  *mylongitude_speed = 0.0;
  *mylatitude_speed = 0.0;

  all[iFollowFace_id].children[(*(int *)myimport("ptencoders","id"))]=FALSE;
  all[iFollowFace_id].children[(*(int *)myimport("ptmotors","id"))]=FALSE;
  ptmotorsstop();
  ptencodersstop();
	colorAstop();

  pthread_mutex_lock(&(all[iFollowFace_id].mymutex));
  put_state(iFollowFace_id,slept);
  printf("iFollowFace: off\n");
  pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));

	free(image);
	free(imgLocalRGB);
	free(imgLocal->imageData);
}

void iFollowFace_terminate()
{
  pthread_mutex_lock(&(all[iFollowFace_id].mymutex));
  iFollowFace_stop();  
  pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));
  sleep(2);
}

void iFollowFace_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[iFollowFace_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }
  pthread_mutex_lock(&(all[iFollowFace_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[iFollowFace_id].children[i]=FALSE;
  all[iFollowFace_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) iFollowFace_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {iFollowFace_brothers[i]=brothers[i];i++;}
    }
  iFollowFace_callforarbitration=fn;
  put_state(iFollowFace_id,notready);
  printf("iFollowFace: on\n");
  iFollowFace_imports();
  colorArun(iFollowFace_id, NULL, NULL);

	image = (char*) malloc(320*240*3);
	imgLocalRGB = (char*) malloc(320*240*3);
  imgLocal = cvCreateImage(cvSize(mycolorAwidth, mycolorAheight), IPL_DEPTH_8U, 1);

	myAllFaces = NULL;
	facesStructCounter = 0;
	systemInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	timeForced = systemInstant;
	completedSearch = FALSE;
	completedTrack = FALSE;
	myActualState = think;
	timeToForcedSearch = TIME_TO_FORCED_SEARCH;
	isForcedSearch = FALSE;
	randomPosition = FALSE;
	nextLatitude = 0;
	nextLongitude = 0;

  pthread_cond_signal(&(all[iFollowFace_id].condition));
  pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));
}

void *iFollowFace_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[iFollowFace_id].mymutex));

      if (all[iFollowFace_id].state==slept)
      {
   iFollowFace_state=init;
   pthread_cond_wait(&(all[iFollowFace_id].condition),&(all[iFollowFace_id].mymutex));
   pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));
      }
      else
      {
   /* check preconditions. For now, preconditions are always satisfied*/
   if (all[iFollowFace_id].state==notready)
      put_state(iFollowFace_id,ready);
   /* check brothers and arbitrate. For now this is the only winner */
   if (all[iFollowFace_id].state==ready)
   {put_state(iFollowFace_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      /* start the winner state from controlled motor values */ 
	      all[iFollowFace_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[iFollowFace_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[iFollowFace_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[iFollowFace_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      ptencodersrun(iFollowFace_id,NULL,NULL);
	      ptmotorsrun(iFollowFace_id,NULL,NULL);
	      encodersrun(iFollowFace_id,NULL,NULL);
	      motorsrun(iFollowFace_id,NULL,NULL);
   gettimeofday(&a,NULL);
   aa=a.tv_sec*1000000+a.tv_usec;
   n=0;
   }

   if (all[iFollowFace_id].state==winner)
      /* I'm the winner and must execute my iteration */
   {
      pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));
      /*      gettimeofday(&a,NULL);*/
      n++;
      iFollowFace_iteration();
      gettimeofday(&b,NULL);
      bb=b.tv_sec*1000000+b.tv_usec;
      pthread_mutex_lock(&iFollowFaceMutex);
      next=aa+(n+1)*(long)iFollowFace_cycle*1000-bb;
      pthread_mutex_unlock(&iFollowFaceMutex);

      if (next>5000)
      {
         usleep(next-5000);
         /* discounts 5ms taken by calling usleep itself, on average */
      }
      else  ;
   }
   else
      /* just let this iteration go away. overhead time negligible */
   {
      pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));
      pthread_mutex_lock(&iFollowFaceMutex);
      usleep(iFollowFace_cycle*1000);
      pthread_mutex_unlock(&iFollowFaceMutex);
   }
      }
   }
}

void iFollowFace_init() {
  pthread_mutex_lock(&(all[iFollowFace_id].mymutex));
  printf("iFollowFace schema started up\n");
  iFollowFace_exports();
  put_state(iFollowFace_id,slept);
  pthread_create(&(all[iFollowFace_id].mythread),NULL,iFollowFace_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[iFollowFace_id].mymutex));

	pthread_mutex_init(&iFollowFaceMutex,PTHREAD_MUTEX_TIMED_NP);
}

void iFollowFace_guibuttons(FL_OBJECT *obj){
}

static int initOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

inline void initCanvasSettings () {
  float r,lati,longi;

	initOGL(664,408);

	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)664/(GLfloat)408,1.0,50000.0);

	/* extrinsic parameters */
  gluLookAt(-2000.,400.,480.,600.,400.,400.,0.,0.,1.);
}

inline void drawFloorInfiniteLines () {
	int i;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f( 0.3, 0.3, 0.3 );
  glBegin(GL_LINES);
  for(i=0;i<((int)MAXWORLD+1);i++)
    {
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,-(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
      v3f((int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
    }
  glEnd();
}

inline void DrawCircle(float cx, float cy, float r, int num_segments) { 
	int ii;
	float x, y, theta;

	glBegin(GL_LINE_LOOP); 
	for(ii = 0; ii < num_segments; ii++) { 
		theta = 2.0f * 3.1415926f * ((float)ii) / ((float)num_segments);//get the current angle 

		x = r * cosf(theta);//calculate the x component 
		y = r * sinf(theta);//calculate the y component 

		glVertex3f(250, -(x + cx), -(y + cy)); // output vertex 

	} 
	glEnd(); 
}

inline void drawDetectedFaces () {
	struct faceStruct *p;
	p = myAllFaces;
	scenePoint myInPoint;
	imagePoint myOutPoint;
	int i = 0;

	while (p != NULL) {
		myInPoint.pan = p->longitude;
		myInPoint.tilt = p->latitude;
		myOutPoint = PT2coordenadas (myInPoint);

		DrawCircle ((float)myOutPoint.x, (float)myOutPoint.y, 20., 5);
		p = p->next;
		i ++;
	}
	// printf ("He pintado %i caras\n", i);
}

static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	initCanvasSettings ();

  /** Robot Frame of Reference **/
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  if (myencoders!=NULL){
     mypioneer.posx=myencoders[0];
     mypioneer.posy=myencoders[1];
     mypioneer.posz=0.;
     mypioneer.foax=myencoders[0];
     mypioneer.foay=myencoders[1];
     mypioneer.foaz=10.;
     mypioneer.roll=myencoders[2]*RADTODEG;
  }
  else{
     mypioneer.posx=0.;
     mypioneer.posy=0.;
     mypioneer.posz=0.;
     mypioneer.foax=0.;
     mypioneer.foay=0.;
     mypioneer.foaz=10.;
     mypioneer.roll=0.;
  }
  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
  dxPioneer = (mypioneer.foax-mypioneer.posx);
  dyPioneer = (mypioneer.foay-mypioneer.posy);
  dzPioneer = (mypioneer.foaz-mypioneer.posz);
  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
  glRotatef (longiPioneer,0.,0.,1.);
  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
  if (rPioneer<0.00001) latiPioneer=0.;
  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
  glRotatef(latiPioneer,0.,1.,0.);
  glRotatef(mypioneer.roll,0.,0.,1.);

	glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
	glPushMatrix();
	glTranslatef(1.,0.,0.);
	/* the body it is not centered. With this translation we center it */
	glScalef (100., 100., 100.);
	loadModel();
	glPopMatrix();
	glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL

	glLoadIdentity ();
	drawFloorInfiniteLines ();
	//drawDetectedFaces (); // TODO

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}


void iFollowFace_guidisplay(){
	int i;

	if (mycolorA != NULL) {
		for (i=0;i<320*240; i++) {
			image[i*3]=(*mycolorA)[i*3+2];
			image[i*3+1]=(*mycolorA)[i*3+1];
			image[i*3+2]=(*mycolorA)[i*3];
		}

		img = cvMat(240, 320, CV_8UC3, *mycolorA);
		RGB2GRAY (&img, imgLocal);
		drawCross (imgLocalRGB, center.x, center.y, 20, 0, 255, 0);
	}
	gdk_threads_enter();
		gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
/*	gdk_threads_enter();
		if (canvas!=NULL) expose_event(canvas, NULL, NULL);
	gdk_threads_leave();*/
}

void iFollowFace_hide(void){
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(iFollowFace_guidisplay);
	display_active=0;
	all[iFollowFace_id].guistate=off;
}

void iFollowFace_show(void){
	static pthread_mutex_t iFollowFace_gui_mutex;
  static int k=0;

	pthread_mutex_lock(&iFollowFace_gui_mutex);
	if (!display_active){
		loadglade ld_fn;
		display_active=1;
		pthread_mutex_unlock(&iFollowFace_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("iFollowFace.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical iFollowFace on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));

		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");
			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

		canvas = glade_xml_get_widget(xml, "canvas");
		gtk_widget_unrealize(canvas);
		if (canvas == NULL) printf ("Canvas is NULL\n");

		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (canvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on canvas\n");
			jdeshutdown(1);
		}

		gtk_widget_set_child_visible (GTK_WIDGET(canvas), TRUE);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&iFollowFace_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}
	gdk_threads_enter();

	// Initialization of the image buffers
	myregister_displaycallback(iFollowFace_guidisplay);
	GdkPixbuf *originalImageBuf, *filteredImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(image,GDK_COLORSPACE_RGB,0,8,320,240,320*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(imgLocalRGB,GDK_COLORSPACE_RGB,0,8,320,240,320*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);

	int a = 0;
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	gdk_threads_leave();
}
