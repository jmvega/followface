/*
 *  Copyright (C) 2007 Javier Martín Ramos 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Javier Martín Ramos <j.ramos@pantuflo.es>
 */

#include "haar.h"

int haar_id=0;
int haar_brothers[MAX_SCHEMAS];
arbitration haar_callforarbitration;

enum haar_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int haar_state;
FD_haargui *fd_haargui;

static CvMemStorage* storage = 0;
static CvMemStorage* storageTmp = 0;
static CvMemStorage* storageAux = 0;
static CvHaarClassifierCascade* cascade = 0;
const char* cascade_name = "haarcascade_frontalface_alt.xml";
IplImage *imgLocal = NULL;
CvMat img; // Envoltura a mycolorA
char *image;
char *imgLocalRGB;
int display_active = 0;
CvSeq *facesTmp, *facesAux;
double detectionTimeAux;
float absoluteDiffX, absoluteDiffY;

/* GTK variables */
GladeXML *xml=NULL; /*Fichero xml*/
GtkWidget *win;
GtkImage *originalImage; // Widgets de las imágenes que se obtienen del Window1 (GTK)
GtkImage *filteredImage;
GtkWidget *canvas;

/* images X server variables */
static XImage *imagenA, *imagenFiltrada;
static char *imagenA_buf, *imagenFiltrada_buf; /* puntero a memoria para la imagen a visualizar en el servidor X. No compartida con el servidor */

registerdisplay myregister_displaycallback;
deletedisplay mydelete_displaycallback;

/* imported variables*/
int mysoncolorA;
unsigned char **mycolorA=NULL;
float *myencoders=NULL;
runFn mycolorAresume;
stopFn mycolorAsuspend;
runFn colorArun;
stopFn colorAstop;
runFn encodersrun, motorsrun;
stopFn encodersstop, motorsstop;
int mycolorAheight;
int mycolorAwidth;
int mycolorAnChannels;

/* exported variables */
int haar_cycle=10; /* ms */
double detectionTime;
CvSeq *faces;
pthread_mutex_t haarMutex;
unsigned long int facesClock;
CvPoint center;
int timeOut;
int comeFromCenter;
double systemInstant;

float angulo_x, angulo_y;
int ahora = TRUE;
int last_movement; // Indica el ultimo movimiento que ha realizado la pantilt, en "local"
int last_full_movement; // Indica el ultimo movimiento que ha realizado la pantilt, en "global"
struct dfilter data_filter;
struct faceStruct* myAllFaces; // vamos a suponer que como mucho tendremos 2 caretos en la escena
int facesStructCounter;

runFn ptmotorsrun, ptencodersrun;
stopFn ptmotorsstop, ptencodersstop;

float *mypan_angle=NULL, *mytilt_angle=NULL;  /* degs */
float *mylongitude=NULL; /* degs, pan angle */
float *mylatitude=NULL; /* degs, tilt angle */
float *mylongitude_speed=NULL;
float *mylatitude_speed=NULL;
float *max_pan=NULL;
float *min_pan=NULL;
float *max_tilt=NULL;
float *min_tilt=NULL;

static SofReference mypioneer;

inline float distancia (imagePoint punto1, imagePoint punto2) {
  return (sqrt (pow((punto2.x-punto1.x), 2.)+pow((punto2.y-punto1.y), 2.)));
}

inline double pasar_radianes_grados (double radianes) {
  return ((radianes * 180.)/PI);
}

inline double pasar_grados_radianes (double grados) {
  return ((grados * PI)/180.);
}

inline scenePoint coordenadas2PT (int x, int y) {
	imagePoint punto1, punto2, punto3, punto_origen;
	t_vector vector1, vector2;
	float dist, ang1, ang2;
	scenePoint c_angulares;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	punto3.x = x; punto3.y = y;
	punto_origen.x = CENTRO_X; punto_origen.y = CENTRO_Y;
	dist =  distancia (punto3, punto_origen);

	c_angulares.tilt = (((dist - punto1.y)*(vector1.x))/(vector1.y))+punto1.x;
	  
	vector1.x = CENTRO_X - x;
	vector1.y = CENTRO_Y - y;
	vector2.x = CENTRO_X - ANCHO_ESCENA_COMPUESTA / 2;
	vector2.y = CENTRO_Y - 0;
	ang1 = atan2 (vector1.x, vector1.y);
	ang2 = atan2 (vector2.x, vector2.y);

	c_angulares.pan = -1*(pasar_radianes_grados (ang2-ang1));

	return c_angulares;
}

inline imagePoint PT2coordenadas (scenePoint PT) {
	imagePoint_float punto1, punto2;
	t_vector_float vector1;
	float radio;
	imagePoint coordenada;

	punto1.x = TILT_MIN; punto1.y = RADIO_MAX;
	punto2.x = TILT_MAX; punto2.y = RADIO_MIN;
	vector1.x = punto2.x - punto1.x;
	vector1.y = punto2.y - punto1.y;

	radio = (vector1.y/vector1.x)*(PT.tilt-punto1.x)+punto1.y;

	coordenada.x = (sin (pasar_grados_radianes(-1*PT.pan)) * radio) + CENTRO_X;
	coordenada.y = CENTRO_Y - (cos (pasar_grados_radianes(-1*PT.pan)) * radio);

	if ((coordenada.x < 0) ||
		(coordenada.x > ANCHO_ESCENA_COMPUESTA) ||
		(coordenada.y < 0) ||
		(coordenada.y > ALTO_ESCENA_COMPUESTA)) {
			coordenada.x = -1;
			coordenada.y = -1;
	}

	return coordenada;
}

inline void drawCross (char *image, int x, int y, int side, unsigned char r, unsigned char g, unsigned char b) {
		int i, offset;

		// linea vertical
		for (i=-side; i<side+1; i++) {
			offset = 320 * (y+i) * 3 + (x * 3);
			if (offset > 0) {
			image[offset + 0] = b;
			image[offset + 1] = g;
			image[offset + 2] = r;
			}
		}

		// linea horizontal
		for (i=-side; i<side+1; i++) {
			offset = 320 * y * 3 + ((x+i) * 3);
			if (offset > 0) {
			image[offset + 0] = b;
			image[offset + 1] = g;
			image[offset + 2] = r;
			}
		}
}

inline void detectaCaras(IplImage *img) {
  static unsigned long int lastClock = 0;
  cvClearMemStorage( storageTmp );
	int i;
/*
  for (i=0;i<320*240; i++){ // recordemos que imgLocal sólo tiene un canal, ya que es en blanco y negro...
		printf ("[%i]\n", (img->imageData)[i]);
	}
*/
  if( cascade && img )
  {
    //detectionTimeAux = (double) cvGetTickCount();
    facesTmp = cvHaarDetectObjects( img, cascade, storageTmp,
                                  1.1, 2, CV_HAAR_DO_CANNY_PRUNING,
                                  cvSize(MIN_EXP_WIDTH, MIN_EXP_HEIGHT) );
		//--------------------------------------------------------------------------
		// A little of general culture :)
		// cvGetTickCount(void): Returns the number of ticks.
		// cvGetTickFrequency(void): Returns the number of ticks per microsecond.
		// Thus, the quotient of cvGetTickCount() and cvGetTickFrequency() will give
		// the number of microseconds starting from the platform-dependent event.
		//--------------------------------------------------------------------------

    //detectionTimeAux = (double) cvGetTickCount() - detectionTimeAux; // ticks to detect face...
    //detectionTimeAux =  detectionTimeAux/((double)cvGetTickFrequency()*1000.); // ...translated into seconds

    //pthread_mutex_lock(&haarMutex);
    //facesClock = lastClock;
    //lastClock++;
    //pthread_mutex_unlock(&haarMutex);
  }
}

inline void dibujaCaras(IplImage* dst) {
   int scale = 1;
   CvRect r;
   CvPoint pt1, pt2;
   int i;

	if (facesTmp->total == 0) {
		center.x = 0;
		center.y = 0;
	} else {
   for( i = 0; i < (facesTmp ? facesTmp->total : 0); i++ ) {
      r = *(CvRect*)cvGetSeqElem(facesTmp, i);
			//if (!((r->x == 0.0) && (r->y == 0.0))) { // si encuentra algún elemento de la secuencia, alguna cara
				//system ("mplayer /usr/share/gnome-games/sounds/bad.ogg > /dev/null");
			//}
//       pt1.x = r->x*scale;
//       pt2.x = (r->x+r->width)*scale;
//       pt1.y = r->y*scale;
//       pt2.y = (r->y+r->height)*scale;
//       cvRectangle(dst, pt1, pt2, CV_RGB(0,0,255), 4, 8, 0);
      //float scale = 1.3;
      //int radius;
      center.x = cvRound((r.x + r.width*0.5));
      center.y = cvRound((r.y + r.height*0.5));
				//printf ("[%i, %i]\n", center.x, center.y);
      //radius = cvRound((r.width + r.height)*0.25*scale);
      //cvCircle( dst, center, radius, CV_RGB(255,0,0), 2, 8, 0 );
   }
	}
}

inline void busqueda_iteration() {  
  *mylongitude_speed=1200*ENCOD_TO_DEG;
  *mylatitude_speed = 0.0;

	//printf ("Entramos en busqueda_it\n");

	if (comeFromCenter) {
	  /* Buscamos hacia la izquierda */
	  if (last_full_movement==right) {
	    if ( *mypan_angle > MAXPAN_NEG/2 ) {
				//printf ("*mylongitude = *min_pan/2;\n");
	      *mylongitude = *min_pan/2;
				timeOut = FALSE;
				printf ("LEFT *mypan_angle = %.2f\n", *mypan_angle);
	    } else { // if ((*mypan_angle > ((MAXPAN_NEG/2)-10)) && (*mypan_angle < ((MAXPAN_NEG/2)+10))) {
				timeOut = TRUE;
	      last_full_movement=left;
				comeFromCenter = FALSE;
				printf ("Se activa condición LEFT\n");
	    }
	  } else { /* Buscamos hacia la derecha */
	    if ( *mypan_angle < MAXPAN_POS/2 ) {
				//printf ("*mylongitude = *max_pan/2;\n");
	      *mylongitude = *max_pan/2;
				timeOut = FALSE;
				printf ("RIGHT *mypan_angle = %.2f\n", *mypan_angle);
	    } else { // if ((*mypan_angle > ((MAXPAN_POS/2)-10)) && (*mypan_angle < ((MAXPAN_POS/2)+10))) {
				timeOut = TRUE;
	      last_full_movement=right;
				comeFromCenter = FALSE;
				printf ("Se activa condición RIGHT\n");
	    }
	  }
	} else { // venimos de algún extremo, así que vamos al centro ahora
		if ((*mypan_angle) != 0) {			
			*mylongitude = 0;
			timeOut = FALSE;
		} else if ((*mypan_angle) == 0) {
			timeOut = TRUE;
			comeFromCenter = TRUE;
		}
	}
}

inline void pantilt_iteration() {
  //printf ("Pantilt Iteration, pixeles=%d(%d) - distancia=%d(%d)\n",data_filter.pixeles,MIN_PIXELES,data_filter.distancia,MIN_DISTANCIA);
//  if (data_filter.distancia > MIN_DISTANCIA) { // Calculamos la distancia que se tiene que mover la PAN-TILT

	    angulo_x = ENCOD_TO_DEG * ((int) abs(data_filter.x) );
      angulo_y = ENCOD_TO_DEG * ((int) abs(data_filter.y) );

      *mylongitude_speed = VEL_MAX_PAN-((POS_MAX_PAN-(abs(data_filter.x)))/((POS_MAX_PAN-POS_MIN_PAN)/(VEL_MAX_PAN-VEL_MIN_PAN)));
      *mylatitude_speed = VEL_MAX_TILT-((POS_MAX_TILT-(abs(data_filter.y)))/((POS_MAX_TILT-POS_MIN_TILT)/(VEL_MAX_TILT-VEL_MIN_TILT)));

      switch (data_filter.cuadrante) {
        /* 
          -----------------------
          |          |          |
          |          |          |
          |    4     |    3     |
          |          |          |
          |--------- |--------- |
          |          |          |
          |          |          |
          |    2     |     1    |
          |          |          |
          -----------------------
        */
        case 1:                  /* Si esta en el 1 -> Mover hacia abajo y derecha */
          // printf("OBJETIVO: Primer Cuadrante\n");
					*mylatitude = -angulo_y + (*mytilt_angle);
					*mylongitude = -angulo_x + (*mypan_angle);
          last_movement=right;
          break;

        case 2:                  /* Si esta en el 2 -> Mover hacia abajo e izquierda */
          // printf("OBJETIVO: Segundo Cuadrante\n");
					*mylatitude = -angulo_y + (*mytilt_angle);
					*mylongitude = angulo_x + (*mypan_angle);
          last_movement=left;
          break;
        case 3:                  /* Si esta en el 3 -> Mover hacia arriba e derecha */
          // printf("OBJETIVO: Tercer Cuadrante\n");
					*mylatitude = angulo_y + (*mytilt_angle);
					*mylongitude = -angulo_x + (*mypan_angle);
          last_movement=right;
          break;

        case 4:                  /*  Si esta en el 4 -> Mover hacia arriba e izquierda */
          // printf("OBJETIVO: Cuarto Cuadrante\n");
					*mylatitude = angulo_y + (*mytilt_angle);
					*mylongitude = angulo_x + (*mypan_angle);
          last_movement=left;
          break;

        default:
          printf("Pantilt Iteration: Cuadrante erroneo\n");
      }
      // printf("Pantilt Iteration: longitude=%f - latitude=%f\n",*mylongitude,*mylatitude);
/*    } else {
      // Paramos los ejes ya que estamos en zona de la banda muerta
      //printf ("Pantilt Iteration: Estamos en zona muerta ... parar ejes\n");
      *mylongitude_speed = 0.0;
      *mylatitude_speed = 0.0;
    }*/
//  }
}

inline void RGB2GRAY (CvMat *img, IplImage *imgOut) {
	int i, j, c, row, k;
	double media;
	CvScalar s;

	for(i=0; i<240*320; i++) {
		c=i%320;
		row=i/320;
		j=row*320+c;

		s=cvGet2D(img,row,c); // obtenemos el valor en el píxel (i,j)
		//printf("[i = %i, j = %i], B=%f, G=%f, R=%f\n", i, j, s.val[0],s.val[1],s.val[2]);

		// Very interesting information :)
		// To convert any color to its most approximate level of gray, first one must obtain the values of its red, green and blue (RGB) primaries.
		// Then it is sufficient to add 30% of the red value plus 59% of that of the green plus 11% of that of the blue, no matter whatever scale is employed.
    // The resultant level is the desired gray value. These percentages are chosen due to the different relative sensitivity of the normal human eye to each 
		// of the primary colors (higher to the green, lower to the blue).

		media=(s.val[0]+s.val[1]+s.val[2])/3;
		s.val[0]=media;
		s.val[1]=media;
		s.val[2]=media;
		cvSet2D(imgOut,row,c,s);

		imgLocalRGB [i*3+0] = media;
		imgLocalRGB [i*3+1] = media;
		imgLocalRGB [i*3+2] = media;
		//printf("[i = %i, j = %i]\n", i, j);
	}

	//if ((center.x > 0) && (center.x < 320) && (center.y > 0) && (center.y < 240))
/*		for (i=0;i<320*240; i++) {
			imgLocalRGB [i*3+0] = (*mycolorA)[i*3+2];
			imgLocalRGB [i*3+1] = (*mycolorA)[i*3+1];
			imgLocalRGB [i*3+2] = (*mycolorA)[i*3+0];
		}*/
}

inline void colorFilter (float *red, float *green, float *blue) { // se supone que filtraremos el color de camiseta del individuo detectado
	// nos desplazamos 60 píxeles del centro de la cara hacia abajo
	int pix_down = 60+center.y;
	int my_x;
	int my_y = pix_down - 20;
	int offset, pixelCounter = 0;
	float myred, mygreen, myblue;

	// recorremos una máscara de 40x40 alrededor del centro escogido
	while ((my_y<240) && (my_y < (center.y + 20))) {
		my_x = center.x - 20;
		while ((my_x<320) && (my_x < (center.x + 20))) {
			offset = my_y*320 + my_x;

			myred += (*mycolorA)[offset*3+2];
			mygreen += (*mycolorA)[offset*3+1];
			myblue += (*mycolorA)[offset*3+0];

			my_x ++;
			pixelCounter ++;
		}
		my_y ++;
	}

	// devolveremos la media de colores de esa máscara de 40x40
	(*red) = (myred / pixelCounter);
	(*green) = (mygreen / pixelCounter);
	(*blue) = (myblue / pixelCounter);
}

inline int pantiltAndFacePos (struct faceStruct *p) {
	if ((*mypan_angle) == 0) { // center
		printf ("Check -- CENTRO\n");
		if (p->scenePos ==1) return (TRUE);
	} else {
		if ((last_full_movement == right) && (comeFromCenter == FALSE)) {
			printf ("Check -- LEFT\n");
			if (p->scenePos == 0) return (TRUE); // left
		} else if ((last_full_movement == left) && (comeFromCenter = FALSE)) {
			printf ("Check -- RIGHT\n");
			if (p->scenePos == 2) return (TRUE); // right
		}
	}

	return FALSE;
}

inline void faceMatching (double actualInstant, float red, float green, float blue) {
	int i = 0, matching;
	scenePoint myScenePoint = coordenadas2PT (center.x, center.y); // obtenemos el punto en escena
	struct faceStruct *myActualFace, *myNewFace, *p, *q;
	myNewFace = NULL;
	myActualFace = NULL;
	p = NULL;
	q = NULL;

	if (facesStructCounter == 0) { // no hemos detectado aún ninguna cara
		myActualFace = (struct faceStruct*) malloc (sizeof (struct faceStruct));

		myActualFace->firstInstant = actualInstant; // instante de detección, en segundos
		myActualFace->lastInstant = myActualFace->firstInstant;
		myActualFace->longitude = *mylongitude;
		myActualFace->latitude = *mylatitude;
		myActualFace->pixel_x = center.x;
		myActualFace->pixel_y = center.y;
		myActualFace->scenePoint_pan = myScenePoint.pan;
		myActualFace->scenePoint_tilt = myScenePoint.tilt;
		myActualFace->red = red;
		myActualFace->green = green;
		myActualFace->blue = blue;
		myActualFace->next = NULL;
		if ((*mypan_angle) == 0) {
			myActualFace->scenePos = 1; // center
			printf ("Creación CENTER\n");
		} else {
			if ((last_full_movement == right) && (comeFromCenter == FALSE)) {
				myActualFace->scenePos = 0; // left
				printf ("Creación LEFT\n");
			} else if ((last_full_movement == left) && (comeFromCenter == FALSE)) {
				myActualFace->scenePos = 2; // right
				printf ("Creación RIGHT\n");
			}
		}

		myAllFaces = myActualFace;
		myActualFace = NULL;

		facesStructCounter ++;
	} else { // ya hay alguna cara insertada en memoria, comparemos con la actual...
		matching = FALSE;
		p = myAllFaces;

		while (p != NULL) {
			q = p;
			if((((actualInstant) - (p->lastInstant)) > TIME_TO_REFRESH_FACES) && (p == myAllFaces) && (pantiltAndFacePos(p)==TRUE)) { // tratamiento de eliminación, si resulta ser el primero de la lista
				// printf ("Eliminando al primero de la lista [%.2f, %.2f]\n", actualInstant, p->lastInstant);
				p = p->next;
				q = q->next;
				myAllFaces = myAllFaces->next;

				facesStructCounter --;
			} else {
				if (!matching) { // si no ha habido aun matching, lo seguimos comprobando
					if ((myScenePoint.pan < p->scenePoint_pan + 10) && (myScenePoint.pan > p->scenePoint_pan - 10 )) {
						if ((myScenePoint.tilt < p->scenePoint_tilt + 10) && (myScenePoint.tilt > p->scenePoint_tilt - 10 )) {
							// printf ("Matching con el primero de la lista\n");
							matching = TRUE;
							p->lastInstant = actualInstant; // instante de detección, en segundos
						}
					}
				}
				
				q = p;
				p = p->next;
				while((p!=NULL) && (!(((actualInstant) - (p->lastInstant)) > TIME_TO_REFRESH_FACES)) && (pantiltAndFacePos(p)==FALSE)) {
					if (!matching) { // si no ha habido aun matching, lo seguimos comprobando
						if ((myScenePoint.pan < p->scenePoint_pan + 10) && (myScenePoint.pan > p->scenePoint_pan - 10 )) {
							if ((myScenePoint.tilt < p->scenePoint_tilt + 10) && (myScenePoint.tilt > p->scenePoint_tilt - 10 )) {
								// printf ("Matching con cualquiera de la lista\n");
								matching = TRUE;
								p->lastInstant = actualInstant; // instante de detección, en segundos
							}
						}
					}
					q = p;
					p = p->next;
				}
				if(p!=NULL) { // hemos salido del bucle porque hemos encontrado una cara desfasada :)
					// printf ("Hemos detectado una cara desfasada -> Eliminando...\n");
					q->next = p->next; // así que la eliminamos, es p
					p = p->next; // preparamos a p para la siguiente vuelta

					facesStructCounter --;
				}
			}
		}

		if (!matching) { // si no ha habido emparejamiento -> es una nueva cara
			myNewFace = (struct faceStruct*) malloc (sizeof (struct faceStruct));

			myNewFace->firstInstant = actualInstant; // instante de detección, en segundos
			myNewFace->lastInstant = myNewFace->firstInstant;
			myNewFace->longitude = *mylongitude;
			myNewFace->latitude = *mylatitude;
			myNewFace->pixel_x = center.x;
			myNewFace->pixel_y = center.y;
			myNewFace->scenePoint_pan = myScenePoint.pan;
			myNewFace->scenePoint_tilt = myScenePoint.tilt;
			myNewFace->red = red;
			myNewFace->green = green;
			myNewFace->blue = blue;
			myNewFace->next = NULL;
			if ((*mypan_angle) == 0) {
				myNewFace->scenePos = 1; // center
				printf ("Creación CENTER\n");
			} else {
				if ((last_full_movement == right) && (comeFromCenter == FALSE)) {
					myNewFace->scenePos = 0; // left
					printf ("Creación LEFT\n");
				} else if ((last_full_movement == left) && (comeFromCenter == FALSE)) {
					myNewFace->scenePos = 2; // right
					printf ("Creación RIGHT\n");
				}
			}

			if (q != NULL)
				q->next = myNewFace;
			else
				myAllFaces = myNewFace;

			myNewFace = NULL;

			facesStructCounter ++;
		}
	}
//  printf ("Total de caras = %i\n", facesStructCounter);
//	printf ("Cara %i - Pix[%i, %i] - Sce[%.2f, %.2f] - R[%.2f], G[%.2f], B[%.2f]\n", facesStructCounter, myAllFaces[i].pixel_x, myAllFaces[i].pixel_y, myAllFaces[i].scenePoint_pan, myAllFaces[i].scenePoint_tilt, myAllFaces[i].red, myAllFaces[i].green, myAllFaces[i].blue);

}

inline void updateFaces () {
	struct faceStruct *p;
	p = myAllFaces;
	int i = 0;
	double instant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

	while (p != NULL) {
		printf ("Actualizando a cara[%i] = [%.2f, %.2f]\n", i, p->scenePoint_pan, p->scenePoint_tilt);
		p->lastInstant = instant;
		p = p->next;
		i ++;
	}
}

void haar_iteration(){
	static char d = 0;
	int i;
	float red, green, blue;
	static unsigned char **mycolorATmp = NULL;
	speedcounter(haar_id);
	double actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

	if (d == 0) {
		cascade = (CvHaarClassifierCascade*)cvLoad( cascade_name, 0, 0, 0 );
		if (!cascade) {
			fprintf(stderr, "Haar: Could not load classifier cascade\n");
			jdeshutdown(1);
		}
		storage = cvCreateMemStorage(0);
		storageTmp = cvCreateMemStorage(0);
	}

	pthread_mutex_lock(&haarMutex);

	if (mycolorA != NULL) {
		if (mycolorA != mycolorATmp) { // Source image has been changed
			int type;
			switch (mycolorAnChannels) {
				case 1: type = CV_8UC1;
				break;
				case 3: type = CV_8UC3;
				break;
				case 4: type = CV_8UC4;
				break;
				default: fprintf(stderr, "Haar: Incorrect number of channels for input image\n");
				jdeshutdown(1);
			}
			img = cvMat(240, 320, type, *mycolorA);

			if (imgLocal != NULL)
				cvReleaseImage(&imgLocal);

			imgLocal = cvCreateImage(cvSize(mycolorAwidth, mycolorAheight), IPL_DEPTH_8U, 1);
			mycolorATmp = mycolorA;
		}

		if (imgLocal != NULL) {
			switch (mycolorAnChannels) {
				case 1: cvCopy(&img, imgLocal, NULL);
				break;
				case 3: RGB2GRAY (&img, imgLocal); // son excluyentes ambas funciones, hacen lo mismo, o usamos una u otra :)
				//cvCvtColor(&img, imgLocal, CV_BGR2GRAY);
				break;
				case 4: cvCvtColor(&img, imgLocal, CV_BGRA2GRAY);
				break;
			}
			pthread_mutex_unlock(&haarMutex);

			detectaCaras(imgLocal);
			dibujaCaras(imgLocal);

			//printf ("Diferencia entre %.2f y %.2f = %.2f\n", actualInstant, systemInstant, actualInstant-systemInstant);

			if (!(((actualInstant) - (systemInstant)) > TIME_TO_CHANGE_SCENE)) { // si no ha pasado el temporizador de la captura actual
				if (facesTmp->total > 0) {
					//colorFilter(&red, &green, &blue); // filtramos el color de la camiseta de tal cara
					faceMatching (actualInstant, red, green, blue); // intentamos cuadrar las caras existentes en escena

					data_filter.x = center.x - 160;
					data_filter.y = center.y - 120;
			    data_filter.distancia = sqrt((data_filter.x*data_filter.x)+(data_filter.y*data_filter.y));

			    // miramos en que cuadrante se encuentra
			    if (data_filter.x > 0)
			      if (data_filter.y > 0)
			        data_filter.cuadrante=1;
				    else
				      data_filter.cuadrante=3;
			    else
				    if (data_filter.y > 0)
				      data_filter.cuadrante=2;
				    else
					    data_filter.cuadrante=4;

				  if ((data_filter.x==0) && (data_filter.y==0))
				    data_filter.cuadrante=0;

					//printf ("%i. [%i, %i]\n", data_filter.cuadrante, center.x, center.y, data_filter.x, data_filter.y);
					do {
						pantilt_iteration();
						actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
						// printf ("%.2f\n", actualInstant-systemInstant);
					} while ((angulo_x < ((*mylongitude) + 1)) && (angulo_x > ((*mylongitude) - 1)) && (angulo_y < ((*mylatitude) + 1)) && (angulo_y > ((*mylatitude) - 1)) && (!((actualInstant-systemInstant) > TIME_TO_CHANGE_SCENE)));
				}
			}	else { // vamos a buscar caras
				if ((timeOut == FALSE) && (((actualInstant) - (systemInstant)) > TIME_TO_CHANGE_SCENE)) {
					updateFaces ();
					busqueda_iteration();
				}
				if (timeOut == TRUE) {
					printf ("WARNING: Igualamos temporizadores\n");
					updateFaces (); // cuando el pantilt para de moverse actualizamos temporizadores de todas las caras que tengamos
					timeOut = FALSE;
					actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
					systemInstant = actualInstant;
				}
			}
		} else
			pthread_mutex_unlock(&haarMutex);
	}
	pthread_mutex_lock(&haarMutex);
	detectionTime = detectionTimeAux;
	CV_SWAP(faces, facesTmp, facesAux);
	CV_SWAP(storage, storageTmp, storageAux);
	pthread_mutex_unlock(&haarMutex);
	d++;
}


/*Importar símbolos*/
void haar_imports(){
	void *aux = NULL;
	/* importamos encoders y motores de la base */
	myencoders=(float *)myimport("encoders","jde_robot");

	motorsrun=(runFn)myimport("motors","run");
	motorsstop=(stopFn)myimport("motors","stop");
	encodersrun=(runFn)myimport("encoders","run");
	encodersstop=(stopFn)myimport("encoders","stop");

	/* importamos motors de pantilt */
  mylongitude=myimport("ptmotors", "longitude");
  mylatitude=myimport ("ptmotors", "latitude");
  mylongitude_speed=myimport("ptmotors", "longitude_speed");
  mylatitude_speed=myimport("ptmotors","latitude_speed");
  
  max_pan=myimport("ptmotors", "max_longitude");
  max_tilt=myimport("ptmotors", "max_latitude");
  min_pan=myimport("ptmotors", "min_longitude");
  min_tilt=myimport("ptmotors", "min_latitude");

	printf ("ptmotors = %f, %f, %f, %f\n", *max_pan, *max_tilt, *min_pan, *min_tilt);

	ptmotorsrun=myimport("ptmotors","run");
	ptmotorsstop=myimport("ptmotors","stop");

  /* importamos encoders de pantilt */
  mypan_angle=myimport("ptencoders", "pan_angle");
  mytilt_angle=myimport("ptencoders", "tilt_angle");

  ptencodersrun=myimport("ptencoders", "run");
  ptencodersstop=myimport("ptencoders", "stop");

	mycolorA = (unsigned char**) myimport("colorA","colorA");
	colorArun = (runFn) myimport("colorA", "run");
	colorAstop	= (stopFn) myimport("colorA", "stop");

	if (mycolorA == NULL || colorArun == NULL || colorAstop == NULL){
	  fprintf(stderr, "I can't import variables from colorA\n");
	  jdeshutdown(1);
	}

  mycolorAheight = 240;
  mycolorAwidth = 320;
  mycolorAnChannels = 3;
}

/*Exportar símbolos*/
void haar_exports(){
  myexport("haar","id",&haar_id);
  myexport("haar","cycle",&haar_cycle);
  myexport("haar","run",(void *)haar_run);
  myexport("haar","stop",(void *)haar_stop);
  myexport("haar", "detectionTime", &detectionTime);
  myexport("haar", "faces", &faces);
  myexport("haar", "haarMutex", &haarMutex);
  myexport("haar", "facesClock", &facesClock);
  myexport("haar", "center", &center);
}

void haar_stop()
{
  *mylongitude_speed = 0.0;
  *mylatitude_speed = 0.0;

  all[haar_id].children[(*(int *)myimport("ptencoders","id"))]=FALSE;
  all[haar_id].children[(*(int *)myimport("ptmotors","id"))]=FALSE;
  ptmotorsstop();
  ptencodersstop();
	colorAstop();

  pthread_mutex_lock(&(all[haar_id].mymutex));
  put_state(haar_id,slept);
  printf("haar: off\n");
  pthread_mutex_unlock(&(all[haar_id].mymutex));

	free(image);
	free(imgLocalRGB);
	free(imgLocal->imageData);
}

void haar_terminate()
{
  pthread_mutex_lock(&(all[haar_id].mymutex));
  haar_stop();  
  pthread_mutex_unlock(&(all[haar_id].mymutex));
  sleep(2);
}

void haar_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[haar_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }
  pthread_mutex_lock(&(all[haar_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[haar_id].children[i]=FALSE;
  all[haar_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) haar_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {haar_brothers[i]=brothers[i];i++;}
    }
  haar_callforarbitration=fn;
  put_state(haar_id,notready);
  printf("haar: on\n");
  haar_imports();
  colorArun(haar_id, NULL, NULL);

	image = (char*) malloc(320*240*3);
	imgLocalRGB = (char*) malloc(320*240*3);
  imgLocal = cvCreateImage(cvSize(mycolorAwidth, mycolorAheight), IPL_DEPTH_8U, 1);

	myAllFaces = NULL;
	facesStructCounter = 0;
	timeOut = FALSE;
	systemInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	comeFromCenter = TRUE;

  pthread_cond_signal(&(all[haar_id].condition));
  pthread_mutex_unlock(&(all[haar_id].mymutex));
}

void *haar_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[haar_id].mymutex));

      if (all[haar_id].state==slept)
      {
   haar_state=init;
   pthread_cond_wait(&(all[haar_id].condition),&(all[haar_id].mymutex));
   pthread_mutex_unlock(&(all[haar_id].mymutex));
      }
      else
      {
   /* check preconditions. For now, preconditions are always satisfied*/
   if (all[haar_id].state==notready)
      put_state(haar_id,ready);
   /* check brothers and arbitrate. For now this is the only winner */
   if (all[haar_id].state==ready)
   {put_state(haar_id,winner);
  			*mylongitude_speed= 0.0;
  			*mylatitude_speed= 0.0;
	      /* start the winner state from controlled motor values */ 
	      all[haar_id].children[(*(int *)myimport("ptencoders","id"))]=TRUE;
	      all[haar_id].children[(*(int *)myimport("ptmotors","id"))]=TRUE;
	      all[haar_id].children[(*(int *)myimport("encoders","id"))]=TRUE;
	      all[haar_id].children[(*(int *)myimport("motors","id"))]=TRUE;
	      ptencodersrun(haar_id,NULL,NULL);
	      ptmotorsrun(haar_id,NULL,NULL);
	      encodersrun(haar_id,NULL,NULL);
	      motorsrun(haar_id,NULL,NULL);
   gettimeofday(&a,NULL);
   aa=a.tv_sec*1000000+a.tv_usec;
   n=0;
   }

   if (all[haar_id].state==winner)
      /* I'm the winner and must execute my iteration */
   {
      pthread_mutex_unlock(&(all[haar_id].mymutex));
      /*      gettimeofday(&a,NULL);*/
      n++;
      haar_iteration();
      gettimeofday(&b,NULL);
      bb=b.tv_sec*1000000+b.tv_usec;
      pthread_mutex_lock(&haarMutex);
      next=aa+(n+1)*(long)haar_cycle*1000-bb;
      pthread_mutex_unlock(&haarMutex);

      if (next>5000)
      {
         usleep(next-5000);
         /* discounts 5ms taken by calling usleep itself, on average */
      }
      else  ;
   }
   else
      /* just let this iteration go away. overhead time negligible */
   {
      pthread_mutex_unlock(&(all[haar_id].mymutex));
      pthread_mutex_lock(&haarMutex);
      usleep(haar_cycle*1000);
      pthread_mutex_unlock(&haarMutex);
   }
      }
   }
}

void haar_init()
{
  pthread_mutex_lock(&(all[haar_id].mymutex));
  printf("haar schema started up\n");
  haar_exports();
  put_state(haar_id,slept);
  pthread_create(&(all[haar_id].mythread),NULL,haar_thread,NULL);
  if (myregister_displaycallback==NULL){
		if ((myregister_displaycallback=(registerdisplay)myimport ("graphics_gtk", "register_displaycallback"))==NULL)
		{
		  printf ("I can't fetch register_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
		if ((mydelete_displaycallback=(deletedisplay)myimport ("graphics_gtk", "delete_displaycallback"))==NULL)
		{
		  printf ("I can't fetch delete_displaycallback from graphics_gtk\n");
		  jdeshutdown(1);
		}
  }

  pthread_mutex_unlock(&(all[haar_id].mymutex));

  // TODO: y esto?? pthread_mutex_init(&haarMutex,PTHREAD_MUTEX_TIMED_NP);

}

void haar_guibuttons(FL_OBJECT *obj){
}

static int initOGL(int w, int h)
 /* Inicializa OpenGL con los parametros que diran como se visualiza. */
{
	GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
	GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
	GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat local_view[] = {0.0};

	glViewport(0,0,(GLint)w,(GLint)h);  
	glDrawBuffer(GL_BACK);
	glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* With this, the pioneer appears correctly, but the cubes don't */
	glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv (GL_LIGHT0, GL_POSITION, position);
	glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
	glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
	glEnable (GL_LIGHT0);
	/*glEnable (GL_LIGHTING);*/

	glEnable(GL_TEXTURE_2D);     /* Enable Texture Mapping */
	glEnable (GL_AUTO_NORMAL);
	glEnable (GL_NORMALIZE);  
	glEnable(GL_DEPTH_TEST);     /* Enables Depth Testing */
	glDepthFunc(GL_LESS);  
	glShadeModel(GL_SMOOTH);     /* Enables Smooth Color Shading */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	return 0;
}

inline void initCanvasSettings () {
  float r,lati,longi;

	initOGL(664,408);

	/* Virtual camera */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity(); 

	/* perspective projection. intrinsic parameters + frustrum */
	gluPerspective(45.,(GLfloat)664/(GLfloat)408,1.0,50000.0);

	/* extrinsic parameters */
  gluLookAt(-2000.,400.,480.,600.,400.,400.,0.,0.,1.);
}

inline void drawFloorInfiniteLines () {
	int i;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f( 0.3, 0.3, 0.3 );
  glBegin(GL_LINES);
  for(i=0;i<((int)MAXWORLD+1);i++)
    {
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,-(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.+(float)i*1000,(int)MAXWORLD*1000/2.,0.);
      v3f(-(int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
      v3f((int)MAXWORLD*1000/2.,-(int)MAXWORLD*1000/2.+(float)i*1000,0.);
    }
  glEnd();
}

inline void DrawCircle(float cx, float cy, float r, int num_segments) { 
	int ii;
	float x, y, theta;

	glBegin(GL_LINE_LOOP); 
	for(ii = 0; ii < num_segments; ii++) { 
		theta = 2.0f * 3.1415926f * ((float)ii) / ((float)num_segments);//get the current angle 

		x = r * cosf(theta);//calculate the x component 
		y = r * sinf(theta);//calculate the y component 

		glVertex3f(250, -(x + cx), -(y + cy)); // output vertex 

	} 
	glEnd(); 
}

inline void drawDetectedFaces () {
	struct faceStruct *p;
	p = myAllFaces;
	scenePoint myInPoint;
	imagePoint myOutPoint;
	int i = 0;

	while (p != NULL) {
		myInPoint.pan = p->longitude;
		myInPoint.tilt = p->latitude;
		myOutPoint = PT2coordenadas (myInPoint);

		DrawCircle ((float)myOutPoint.x, (float)myOutPoint.y, 20., 5);
		p = p->next;
		i ++;
	}
	// printf ("He pintado %i caras\n", i);
}

static gboolean expose_event (GtkWidget *widget, GdkEventExpose *event, gpointer data) {
	GdkGLContext *glcontext;
	GdkGLDrawable *gldrawable;
	static pthread_mutex_t gl_mutex;
	float dxPioneer, dyPioneer, dzPioneer, longiPioneer, latiPioneer, rPioneer;

	pthread_mutex_lock(&gl_mutex);

	glcontext = gtk_widget_get_gl_context (widget);
	gldrawable = gtk_widget_get_gl_drawable (widget);

	if (!gdk_gl_drawable_gl_begin (gldrawable, glcontext)){
	  pthread_mutex_unlock(&gl_mutex);
	  return FALSE;
	}

	initCanvasSettings ();

  /** Robot Frame of Reference **/
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  if (myencoders!=NULL){
     mypioneer.posx=myencoders[0];
     mypioneer.posy=myencoders[1];
     mypioneer.posz=0.;
     mypioneer.foax=myencoders[0];
     mypioneer.foay=myencoders[1];
     mypioneer.foaz=10.;
     mypioneer.roll=myencoders[2]*RADTODEG;
  }
  else{
     mypioneer.posx=0.;
     mypioneer.posy=0.;
     mypioneer.posz=0.;
     mypioneer.foax=0.;
     mypioneer.foay=0.;
     mypioneer.foaz=10.;
     mypioneer.roll=0.;
  }
  glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
  dxPioneer = (mypioneer.foax-mypioneer.posx);
  dyPioneer = (mypioneer.foay-mypioneer.posy);
  dzPioneer = (mypioneer.foaz-mypioneer.posz);
  longiPioneer = (float)atan2(dyPioneer,dxPioneer)*360./(2.*PI);
  glRotatef (longiPioneer,0.,0.,1.);
  rPioneer = sqrt(dxPioneer*dxPioneer+dyPioneer*dyPioneer+dzPioneer*dzPioneer);
  if (rPioneer<0.00001) latiPioneer=0.;
  else latiPioneer=acos(dzPioneer/rPioneer)*360./(2.*PI);
  glRotatef(latiPioneer,0.,1.,0.);
  glRotatef(mypioneer.roll,0.,0.,1.);

	glEnable (GL_LIGHTING); // LUCES Y.... ACCION!!
	glPushMatrix();
	glTranslatef(1.,0.,0.);
	/* the body it is not centered. With this translation we center it */
	glScalef (100., 100., 100.);
	loadModel();
	glPopMatrix();
	glDisable (GL_LIGHTING); // FUERA LUCES, VOLVEMOS A LA VIDA REAL

	glLoadIdentity ();
	drawFloorInfiniteLines ();
	drawDetectedFaces (); // TODO

	if (gdk_gl_drawable_is_double_buffered (gldrawable)){
	  gdk_gl_drawable_swap_buffers (gldrawable);
	}
	else{
	  glFlush ();
	}

	gdk_gl_drawable_gl_end (gldrawable);

	pthread_mutex_unlock(&gl_mutex);
	return TRUE;
}


void haar_guidisplay(){
	int i;

	if (mycolorA != NULL) {
		for (i=0;i<320*240; i++) {
			image[i*3]=(*mycolorA)[i*3+2];
			image[i*3+1]=(*mycolorA)[i*3+1];
			image[i*3+2]=(*mycolorA)[i*3];
		}

		img = cvMat(240, 320, CV_8UC3, *mycolorA);
		RGB2GRAY (&img, imgLocal);
//		drawCross (imgLocalRGB, center.x, center.y, 20, 0, 255, 0);
	}

	
	gdk_threads_enter();
		gtk_widget_queue_draw(GTK_WIDGET(win));
	gdk_threads_leave();
	gdk_threads_enter();
		if (canvas!=NULL) expose_event(canvas, NULL, NULL);
	gdk_threads_leave();

}

void haar_hide(void){
  if (win!=NULL) {
      gdk_threads_enter();
      gtk_widget_hide(win);
      gdk_threads_leave();
	}
  mydelete_displaycallback(haar_guidisplay);
	display_active=0;
	all[haar_id].guistate=off;
}

void haar_show(void){
	static pthread_mutex_t haar_gui_mutex;
  static int k=0;

	pthread_mutex_lock(&haar_gui_mutex);
	if (!display_active){
		loadglade ld_fn;
		display_active=1;
		pthread_mutex_unlock(&haar_gui_mutex);

		/*Load the window from the .glade xml file*/
		gdk_threads_enter();  
		if ((ld_fn=(loadglade)myimport("graphics_gtk","load_glade"))==NULL){
		    fprintf (stderr,"I can't fetch 'load_glade' from 'graphics_gtk'.\n");
		    jdeshutdown(1);
		}

		xml = ld_fn ("haar.glade");
		if (xml==NULL){
		    fprintf(stderr, "Error loading graphical haar on xml\n");
		    jdeshutdown(1);
		}

		// Set OpenGL Parameters
		GdkGLConfig *glconfig;
		/* Try double-buffered visual */
		glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH |	GDK_GL_MODE_DOUBLE));

		if (glconfig == NULL)  {
			g_print ("*** Cannot find the double-buffered visual.\n");
			g_print ("*** Trying single-buffered visual.\n");
			/* Try single-buffered visual */
			glconfig = gdk_gl_config_new_by_mode ((GdkGLConfigMode)(GDK_GL_MODE_RGB |	GDK_GL_MODE_DEPTH));
			if (glconfig == NULL) {
				g_print ("*** No appropriate OpenGL-capable visual found.\n");
				jdeshutdown(1);
			}
		}

		canvas = glade_xml_get_widget(xml, "canvas");
		gtk_widget_unrealize(canvas);
		if (canvas == NULL) printf ("Canvas is NULL\n");

		// Set OpenGL-capability to the widget
		if (gtk_widget_set_gl_capability (canvas,	glconfig,	NULL,	TRUE,	GDK_GL_RGBA_TYPE)==FALSE) {
			printf ("No Gl capability on canvas\n");
			jdeshutdown(1);
		}

		gtk_widget_set_child_visible (GTK_WIDGET(canvas), TRUE);

		// CONNECT CALLBACKS
		win = glade_xml_get_widget(xml, "window1");
		glade_xml_signal_autoconnect (xml); // Conectar los callbacks

		if (win==NULL){
			fprintf(stderr, "Error loading graphic interface\n");
			jdeshutdown(1);
		} else {
			gtk_widget_show(win);
			gtk_widget_queue_draw(GTK_WIDGET(win));
		}
		gdk_threads_leave();
	} else {
		pthread_mutex_unlock(&haar_gui_mutex);
		gdk_threads_enter();
		gtk_widget_show(win);
		gtk_widget_queue_draw(GTK_WIDGET(win));
		gdk_threads_leave();
	}
	gdk_threads_enter();

	// Initialization of the image buffers
	myregister_displaycallback(haar_guidisplay);
	GdkPixbuf *originalImageBuf, *filteredImageBuf;

	originalImage = GTK_IMAGE(glade_xml_get_widget(xml, "originalImage"));
	filteredImage = GTK_IMAGE(glade_xml_get_widget(xml, "filteredImage"));

	originalImageBuf = gdk_pixbuf_new_from_data(image,GDK_COLORSPACE_RGB,0,8,320,240,320*3,NULL,NULL);
	filteredImageBuf = gdk_pixbuf_new_from_data(imgLocalRGB,GDK_COLORSPACE_RGB,0,8,320,240,320*3,NULL,NULL);

	gtk_image_set_from_pixbuf(originalImage, originalImageBuf);
	gtk_image_set_from_pixbuf(filteredImage, filteredImageBuf);

	int a = 0;
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);

	gdk_threads_leave();
}
