/*
 *  Copyright (C) 2007 Javier Martín Ramos
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Javier Martín Ramos <j.ramos@pantuflo.es>
 */

#include "followface.h"

int followface_id=0;
int followface_brothers[MAX_SCHEMAS];
arbitration followface_callforarbitration;

enum followface_states {init,t1,r1,t2,r2,t3,r3,t4,end};
static int followface_state;
FD_followfacegui *fd_followfacegui;
int display_active = 0;

CvMat imgMyColorA;
IplImage *imgWorkingA = NULL;
IplImage *maskColorVerosimil = NULL;
IplImage *maskNotColor = NULL;
IplImage *imgWorkingA_H = NULL;
IplImage *imgWorkingA_V = NULL;
IplImage *backproject = NULL;
IplImage *imgPreviousWorkingA_V = NULL;
IplImage *pyramid = NULL;
IplImage *prevPyramid = NULL;
IplImage *imgWorkingA_HSV = NULL;
IplImage *tmp = NULL;
CvHistogram *hist = 0;
char haarDetector = 0;
char ipDetector = 0;
CvSeq *faces;
char verFiltro = 0; // boolean

int hdims = 16;
float hranges_arr[] = {0,180};
float* hranges = hranges_arr;

#define HORIZONTAL_ANGLE 65
#define VERTICAL_ANGLE 50
#define CAMSHIFT 0

#define MAX_PUNTOS 50
#define WIN_SIZE 7
   /* Por motivos de funcionamiento de la funcion cvFindCornerSubPix
      ver linea 92 en cvcornersubpix.cpp
      MIN((MIN_EXP_WIDTH - 5)/2, (MIN_EXP_HEIGHT - 5)/2) */

unsigned short skinlikelyhsv[6]; // hmin, hmax, smin, smax, vmin, vmax

/* exported variables */
int followface_cycle=1000/25; /* ms */

/* imported variables */
   float *mylongitude = NULL;
   float *mylatitude = NULL;
   float *mylongitude_speed = NULL;
   float *mylatitude_speed = NULL;

   float *myzoom = NULL;
   float *myzoom_speed = NULL;

   float *myzoom_position = NULL;

   float *mypan_angle = NULL;
   float *mytilt_angle = NULL;

   float *myminlongitude = NULL;
   float *myminlatitude = NULL;
   float *mymaxlongitude = NULL;
   float *mymaxlatitude = NULL;
   float *mymaxlongitude_speed = NULL;
   float *mymaxlatitude_speed = NULL;
   float *mymaxzoom_speed = NULL;
   float *mymaxzoom = NULL;
   float *myminzoom = NULL;

   int mysonptmotors, mysonptencoders;
   runFn ptmotorsresume, ptencodersresume = NULL;
   stopFn ptmotorssuspend, ptencoderssuspend = NULL;

   int mysonzmotors, mysonzencoders;
   runFn zmotorsresume, zencodersresume = NULL;
   stopFn zmotorssuspend, zencoderssuspend = NULL;

   float x = 0, y = 0, z = 0;
   float x_speed = 0, y_speed = 0, z_speed = 0;

   int mysoncolorA;
   unsigned char **mycolorA=NULL;
   runFn mycolorAresume;
   stopFn mycolorAsuspend;
   int mycolorAheight;
   int mycolorAwidth;

   int mySonHaar;
   runFn myHaarResume;
   stopFn myHaarSuspend;
   CvSeq **myHaarFaces;
   pthread_mutex_t * myHaarMutex;
   unsigned long int *myHaarClock;
   int *myHaarCycle;
   double *myHaarDetectionTime;

   inline void capturaImagen(char camara){
      if (camara == 'A') {
         int i;
         CvArr *p_img = &imgMyColorA;
//          cvCopy(p_img, imgWorkingA, NULL);
         for (i = 0; i < mycolorAheight*mycolorAwidth*3; i++) {
            imgWorkingA->imageData[i] = imgMyColorA.data.ptr[i];
         }
         cvCvtColor(imgWorkingA, imgWorkingA_HSV, CV_BGR2HSV);

         cvSplit(imgWorkingA_HSV, imgWorkingA_H, NULL, NULL, NULL);
         cvSplit(imgWorkingA_HSV, NULL, NULL, imgWorkingA_V, NULL);
      }
   }
   
   inline void creaImagenes(void) {
      CvSize frameSize = cvSize(mycolorAwidth,mycolorAheight);
      imgMyColorA = cvMat(mycolorAheight, mycolorAwidth, CV_8UC3, *mycolorA);

      if (imgWorkingA != NULL)
         cvReleaseImage(&imgWorkingA);
      imgWorkingA = cvCreateImage(frameSize, IPL_DEPTH_8U, 3);


      imgWorkingA_V = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);

      imgWorkingA_HSV = cvCreateImage(frameSize, IPL_DEPTH_8U, 3);

      imgWorkingA_H = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);

      imgPreviousWorkingA_V = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);
      pyramid = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);
      prevPyramid = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);

      backproject = cvCreateImage( frameSize, 8, 1 );

   // Mascaras de filtros. En bitonal.
      maskColorVerosimil = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);
      maskNotColor = cvCreateImage(frameSize, IPL_DEPTH_8U, 1);

   }

inline unsigned short entre(unsigned short val, unsigned short ini,
                              unsigned short fin)
{
   if((ini <= val) && (fin >= val))
      return 255;
   else
      return 0;
}
   
int filtroHSV(IplImage *img, unsigned short hsv[6], IplImage *mask_out) {
/* PRE: mask_out es del mismo tamaño que img, con profundidad de un byte
   hsv contiene los valores hmin, hmax, smin, smax, vmin, vmax en ese orden */
   int i;
   unsigned short hmin, hmax, complementa; // el canal h es circular
   unsigned char h, s, v;
   if (hsv[0] > hsv[1]) {
      hmin = hsv[1];
      hmax = hsv[0];
      complementa = 255;
   }
   else {
      hmin = hsv[0];
      hmax = hsv[1];
      complementa = 0;
   }
   for (i = 0; i < mycolorAheight*mycolorAwidth; i++) {
      h = img->imageData[3*i+0];
      s = img->imageData[3*i+1];
      v = img->imageData[3*i+2];
      mask_out->imageData[i] = (entre(h, hmin, hmax) ^ complementa) &
            entre(s,hsv[2],hsv[3]) & entre(v,hsv[4],hsv[5]);
   }
   cvDilate(mask_out, mask_out, NULL, 2);
   return cvCountNonZero(mask_out);
}

inline CvPoint pEsfericas(CvPoint p) {
   CvPoint retorno;
   retorno.x = *mypan_angle + (p.x/(float)mycolorAwidth * HORIZONTAL_ANGLE) - HORIZONTAL_ANGLE/2.;
   retorno.y = -(*mytilt_angle + (p.y/(float)mycolorAheight * VERTICAL_ANGLE) - VERTICAL_ANGLE/2.);
   return retorno;
}

inline CvRect rEsfericas(CvRect r) {
   CvRect retorno;
   CvPoint a, b;
   a = pEsfericas( cvPoint(r.x, r.y) );
   b = pEsfericas( cvPoint(r.x + r.width, r.y + r.height) );
   retorno.x = a.x;
   retorno.y = a.y;
   retorno.width = b.x - a.x;
   retorno.height = a.y - b.y;
   return retorno;
}

inline float v1(float diff, float d, float maxArea, float area) {
   return fabs(diff-d)/(mycolorAwidth/2. -d) * 4. * maxArea/area;
}

inline float v2(float diff, float d, float maxArea, float area) {
   return fabs(diff-d)/(mycolorAwidth/2. -d) * 6. * maxArea/area;
}

inline void calcMovementToTarget(CvPoint t, CvRect r, int d, float *pan, 
   float *tilt, float *panSpeed, float *tiltSpeed)
{
   // PRE: t es el centro de r
   // Face-driven
   float diff, area;
   float maxArea = powf( MIN(mycolorAheight, mycolorAwidth), 2 );
   CvPoint targetOnPT, targetOnImage;

   // La esquina más alejada de la regiónr será el objetivo
   if (t.x < mycolorAwidth/2) {
      *pan = *myminlongitude;
      targetOnImage.x = r.x;
   }
   else if (t.x > mycolorAwidth/2) {
      *pan = *mymaxlongitude;
      targetOnImage.x = r.x + r.width;
   }
   if (t.y < mycolorAheight/2) {
      *tilt = *mymaxlatitude;
      targetOnImage.y = r.y;
   }
   else if (t.y > mycolorAheight/2) {
      *tilt = *myminlatitude;
      targetOnImage.y = r.y + r.height;
   }

   targetOnPT = pEsfericas(targetOnImage);

   // Posición destino
//    *pan = targetOnPT.x;
//    *tilt = targetOnPT.y;

   area = r.width * r.height;
   diff = abs(t.x - mycolorAwidth/2);
   if ( diff < d ) {
      *panSpeed = 0;
   }
   else if ( diff < 3.*d/2. ) {
      *panSpeed = 10;
   }
   else if ( diff < 2.*d) {
      *panSpeed = 10 + v1(diff, 3.*d/2., maxArea, area);
   }
   else {
      *panSpeed = 10 + v1(diff, 3.*d/2., maxArea, area) + v2(diff, 2.*d, mycolorAwidth, r.width);
   }

   diff = abs(t.y - mycolorAheight/2);
   if ( diff < d ) {
      *tiltSpeed = 0;
   }
   else if ( diff < 3.*d/2. ) {
      *tiltSpeed = 10;
   }
   else if ( diff < 2.*d) {
      *tiltSpeed = 10 + v1(diff, 3.*d/2., maxArea, area);
   }
   else {
      *tiltSpeed = 10 + v1(diff, 3.*d/2., maxArea, area) + v2(diff, 2.*d, mycolorAheight, r.height);
   }

   if (CAMSHIFT) {
      *panSpeed = MIN(60, *panSpeed);
      *tiltSpeed = MIN(40, *tiltSpeed);
   }
   else {
      *panSpeed = MIN(40, *panSpeed);
      *tiltSpeed = MIN(40, *tiltSpeed);
   }
   printf("%f %f\n", *panSpeed, *tiltSpeed);
}

inline void calcManualMovement(float *pan, float *tilt, float *panSpeed,
   float *tiltSpeed)
{
   // GUI-driven
   *pan = *myminlongitude + x*(*mymaxlongitude - *myminlongitude);
   *tilt = *myminlatitude + y*(*mymaxlatitude - *myminlatitude);
   *panSpeed = x_speed * *mymaxlongitude;
   *tiltSpeed = y_speed * *mymaxlatitude;
}

inline void move(float panPos, float tiltPos, float panSpeed, float tiltSpeed) {
   if (!panSpeed) {
      panPos = *mypan_angle;
      panSpeed = 300;
   }
   *mylongitude = panPos;
   *mylongitude_speed = panSpeed;
   if (!tiltSpeed) {
      tiltPos = *mytilt_angle;
      tiltSpeed = 125;
   }
   *mylatitude = tiltPos;
   *mylatitude_speed = tiltSpeed;
}

inline float solapaBase(CvRect i, CvRect j) {
   return MAX(0, MIN(i.x + i.width, j.x + j.width) - MAX(i.x, j.x));
}

inline float solapaAltura(CvRect i, CvRect j) {
   return MAX(0, MIN(i.y + i.height, j.y + j.height) - MAX(i.y, j.y));
}

inline float solapa(CvRect i, CvRect j) {
   float sBase = solapaBase(i, j);
   float sAltura = solapaAltura(i, j);
   float denom = i.width * i.height;

   return (sBase * sAltura)/denom;
}

inline float proximidad(CvRect i, CvRect j) {
   return MIN(solapa(i, j), solapa(j, i));
}

inline void setBiggestTarget(CvSeq *faces, CvPoint *t, CvRect *r) {
   int i;
   CvRect rCara;
   CvPoint centro;
   CvRect parcialR = *r;
   float areaMax = 0, area;

   for (i = 0; i < faces->total; i++) {
      rCara = *(CvRect*)cvGetSeqElem(faces, i);
      centro = cvPoint(rCara.x + rCara.width/2, rCara.y + rCara.height/2);
      area = rCara.width * rCara.height;
      if (area > areaMax) {
         areaMax = area;
         parcialR = rCara;
      }
   }
   *r = parcialR;
   *t = cvPoint(parcialR.x + parcialR.width/2, parcialR.y + parcialR.height/2);
}

inline void setClosestTargetOnPT(CvSeq *faces, CvPoint *tPT, CvRect *r, CvPoint *t) {
   int i;
   CvRect rCara;
   CvPoint centro, centroPT;
   CvPoint parcial = *t;
   CvRect parcialR = *r;
   CvPoint parcialPT = *tPT;
   float distMin, dist;

   for (i = 0; i < faces->total; i++) {
      rCara = *(CvRect*)cvGetSeqElem(faces, i);
      centro = cvPoint(rCara.x + rCara.width/2, rCara.y + rCara.height/2);
      centroPT = pEsfericas(centro);
      dist = abs(tPT->x - centroPT.x) + abs(tPT->y - centroPT.y);
      if ((dist < distMin) || (i == 0)) {
         parcialPT = centroPT;
         parcial = centro;
         distMin = dist;
         parcialR = rCara;
      }
   }
   *r = parcialR;
   *t = parcial;
   *tPT = parcialPT;
}

inline void setClosestTarget(CvSeq *faces, CvPoint *t, CvRect *r) {
   int i;
   CvRect rCara;
   CvPoint centro;
   CvPoint parcialP = *t;
   CvRect parcialR = *r;
   float proxMax = 0, prox;

   for (i = 0; i < faces->total; i++) {
      rCara = *(CvRect*)cvGetSeqElem(faces, i);
      centro = cvPoint(rCara.x + rCara.width/2, rCara.y + rCara.height/2);
      prox = proximidad(*r, rCara);
      if (prox > proxMax) {
         parcialP = centro;
         proxMax = prox;
         parcialR = rCara;
      }
   }
   *r = parcialR;
   *t = parcialP;
   if (proxMax == 0)
      setBiggestTarget(faces, t, r);
}

inline void getHistogramFromTarget(CvRect selection){
   float max_val = 0.f;
   cvSetImageROI( imgWorkingA_H, selection );
   cvSetImageROI( maskColorVerosimil, selection );
   cvCalcHist( &imgWorkingA_H, hist, 0, maskColorVerosimil );
   cvGetMinMaxHistValue( hist, 0, &max_val, 0, 0 );
   cvConvertScale( hist->bins, hist->bins, max_val ? 255. / max_val : 0., 0 );
   cvResetImageROI( imgWorkingA_H );
   cvResetImageROI( maskColorVerosimil );
}

inline void calcFlow(CvPoint2D32f *p, CvPoint2D32f *tmp, int *n, unsigned char *status,
   CvPoint *t)
{
   int i, k;
   int flags = 0;
   CvPoint2D32f *aux;

   cvCalcOpticalFlowPyrLK(imgPreviousWorkingA_V,
                     imgWorkingA_V,
                     prevPyramid,
                     pyramid,
                     p,
                     tmp,
                     *n,
                     cvSize(WIN_SIZE,WIN_SIZE),
                     3,
                     status,
                     0,
                     cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03),
                     flags);
   flags |= CV_LKFLOW_PYR_A_READY;

   CV_SWAP(tmp, p, aux);

   for( i = k = 0; i < MAX_PUNTOS; i++ )
   {
      if( !status[i] )
         continue;

      p[k++] = p[i];
   }

   *t = cvPoint(0, 0);
   for ( i = 0; i < *n; i++) {
      if (status[i]) {
         t->x += p[i].x;
         t->y += p[i].y;
      }
   }

   if (*n) {
      t->x /= *n;
      t->y /= *n;
   }
   else {
      t->x = mycolorAwidth/2;
      t->y = mycolorAheight/2;
   }
}

void distribuyePuntos(CvRect re, CvPoint2D32f *puntos, int *nPuntos) {
   IplImage *eig;
   IplImage *temp;
   double quality = 0.01;
   double min_distance = 3;
   int i;

   cvSetImageROI(imgWorkingA_V, re);
   eig = cvCreateImage( cvGetSize(imgWorkingA_V), 32, 1 );
   temp = cvCreateImage( cvGetSize(imgWorkingA_V), 32, 1 );

   *nPuntos = MIN(MAX_PUNTOS, re.width/2);
   cvGoodFeaturesToTrack(imgWorkingA_V,
                           eig,
                           temp,
                           puntos,
                           nPuntos,
                           quality,
                           min_distance,
                           0,
                           3,
                           0,
                           0.04);
   cvFindCornerSubPix( imgWorkingA_V,
                     puntos,
                     *nPuntos,
                     cvSize(WIN_SIZE,WIN_SIZE), cvSize(-1,-1),
                     cvTermCriteria(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03));
   cvReleaseImage( &eig );
   cvReleaseImage( &temp );
   cvResetImageROI(imgWorkingA_V);
   for (i = 0; i < *nPuntos; i++) {
      puntos[i].x += re.x;
      puntos[i].y += re.y;
   }
}

inline void dibujaCaras(IplImage* dst) {
   int scale = 1;
   CvRect* r;
   CvPoint pt1, pt2;
   int i;

   for( i = 0; i < (faces ? faces->total : 0); i++ ) {
      r = (CvRect*)cvGetSeqElem(faces, i);
//       pt1.x = r->x*scale;
//       pt2.x = (r->x+r->width)*scale;
//       pt1.y = r->y*scale;
//       pt2.y = (r->y+r->height)*scale;
//       cvRectangle(dst, pt1, pt2, CV_RGB(0,0,255), 4, 8, 0);
      CvPoint center;
      int radius;
      center.x = cvRound((r->x + r->width*0.5));
      center.y = cvRound((r->y + r->height*0.5));
      radius = cvRound((r->width + r->height)*0.25);
      cvCircle( dst, center, radius, CV_RGB(255,0,0), 2, 8, 0 );
   }
}
CvRect target;
CvPoint targetCentre;
CvPoint targetCentreOnPT;
double estimatedTimeRemaining;
void followface_iteration(){
   static int d = 0;
   static unsigned long int haarLastClock = 0, ipLastClock=0;
   int mediaVentana = MIN(mycolorAheight, mycolorAwidth)/6;
   static float panPos, tiltPos, panSpeed, tiltSpeed;
   static CvPoint2D32f puntos[MAX_PUNTOS];
   static CvPoint2D32f tempPuntos[MAX_PUNTOS];
   static unsigned char estadoPuntos[MAX_PUNTOS];
   static int nPuntos = 0;
   static char track = 0; // boolean
   static CvBox2D track_box;
   static CvConnectedComp track_comp;
   speedcounter(followface_id);

   if (d == 0) {
      creaImagenes();
      hist = cvCreateHist( 1, &hdims, CV_HIST_ARRAY, &hranges, 1 );
      followface_exports();
      d++;
      targetCentre = cvPoint(mycolorAwidth/2, mycolorAheight/2);
      bzero(estadoPuntos, MAX_PUNTOS);
   }

   capturaImagen('A');
   filtroHSV(imgWorkingA_HSV, skinlikelyhsv, maskColorVerosimil);
   if (verFiltro)
      cvSet(imgWorkingA, cvScalar(0, 255, 0, 0), maskColorVerosimil);

   cvAnd(imgWorkingA_V, maskColorVerosimil, imgWorkingA_V, NULL);
   estimatedTimeRemaining -= 1000/followface_cycle;

   if (haarDetector) {
		printf ("haarDetector activated\n");
      if (estimatedTimeRemaining < 0) {
         if (pthread_mutex_trylock(myHaarMutex) == 0) {
            if (haarLastClock != *myHaarClock) {
               faces = *myHaarFaces;
               haarLastClock = *myHaarClock;
               if ((*myHaarFaces)->total && !track) {
                  setBiggestTarget(faces, &targetCentre, &target);
                  targetCentreOnPT = pEsfericas(targetCentre);
                  track = 1;
               }
               if (track) {
                  if (CAMSHIFT) {
                     setClosestTarget(faces, &targetCentre, &target);
                     getHistogramFromTarget(target);
                  }
                  else {
                     setClosestTargetOnPT(faces, &targetCentreOnPT, &target, &targetCentre);
                  }
               }
               estimatedTimeRemaining = *myHaarDetectionTime;
//                distribuyePuntos(target, puntos, &nPuntos);
               dibujaCaras(imgWorkingA);
            }
            pthread_mutex_unlock(myHaarMutex);
         }
         else
            estimatedTimeRemaining = 2*1000/followface_cycle;
      }
      else {
//          calcFlow(puntos, tempPuntos, &nPuntos, estadoPuntos, &targetCentre);
      }

      if (track && CAMSHIFT) {
         cvCalcBackProject( &imgWorkingA_H, backproject, hist );
         cvAnd( backproject, maskColorVerosimil, backproject, 0 );
         cvCamShift( backproject, target,
                     cvTermCriteria( CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 10, 1 ),
                     &track_comp, &track_box );
         target = track_comp.rect;
         targetCentre = cvPoint(target.x + target.width/2, target.y + target.height/2);
      }

      calcMovementToTarget(targetCentre, target, mediaVentana, &panPos, 
                           &tiltPos, &panSpeed, &tiltSpeed);
   }
   else {
      track = 0;
      targetCentre = cvPoint(mycolorAwidth/2, mycolorAheight/2);
      calcManualMovement(&panPos, &tiltPos, &panSpeed, &tiltSpeed);
   }

   move(panPos, tiltPos, panSpeed, tiltSpeed);

   if (display_active) {
      CvPoint pt1, pt2;
      int i;

      for (i = 0; i < nPuntos; i++) {
         if (estadoPuntos[i]) {
            cvCircle(imgWorkingA, cvPointFrom32f(puntos[i]), 2, CV_RGB(0,255,0),
                     -1, 8, 0);
         }
      }

      pt1.x = mycolorAwidth/2 - mediaVentana;
      pt2.x = mycolorAwidth/2 + mediaVentana;
      pt1.y = mycolorAheight/2 - mediaVentana;
      pt2.y = mycolorAheight/2 + mediaVentana;
      cvRectangle(imgWorkingA, pt1, pt2, CV_RGB(255,0,0), 1, 8, 0);

      if (track)
         cvEllipseBox( imgWorkingA, track_box, CV_RGB(0,0,255), 1, CV_AA, 0 );

      cvCircle(imgWorkingA, targetCentre, 2, CV_RGB(255,0,0), -1, 8, 0);
//       cvCircle(imgWorkingA, targetCentre, target.width/2, CV_RGB(255,0,0), 2, 8, 0 );

      cvShowImage( "followface", imgWorkingA);
      cvWaitKey(10);
   }

   CV_SWAP(prevPyramid, pyramid, tmp);
   CV_SWAP(imgPreviousWorkingA_V, imgWorkingA_V, tmp);

  //Watch PT
//   if (mypan_angle) {
//      printf("pan: %f ", *mypan_angle);
//   }
//   if (mytilt_angle) {
//      printf("tilt: %f ", *mytilt_angle);
//   }
//   //Watch Z
//   if (myzoom_position) {
//      printf("zoom: %f ", *myzoom_position);
//   }
// 
//   if (mypan_angle || mytilt_angle || myzoom_position)
//      printf("\n");
}


/*Importar símbolos*/
void followface_imports(){
   int *aux = NULL;
	int mylittleWidth = 320;
  int mylittleHeight = 240;

   aux = (int *)myimport("colorA","id");
   if (aux) {
      mysoncolorA=*aux;
      mycolorA=(unsigned char **) myimport("colorA", "colorA");
	if (!mycolorA) {
		printf ("mycolorA null\n");
  }

      mycolorAresume=(runFn)myimport("colorA", "run");
	if (!mycolorAresume) {
		printf ("mycolorAresume null\n");
  }

      mycolorAsuspend=(stopFn)myimport("colorA","stop");
	if (!mycolorAsuspend) {
		printf ("mycolorAsuspend null\n");
  }

      aux = (int *)myimport("colorA","height");
      if (aux)
         mycolorAheight = *aux;
      else
         mycolorAheight = mylittleHeight;
	if (!aux) {
		printf ("mycolorAheight null\n");
  }

      aux = (int *)myimport("colorA","width");
      if (aux)
         mycolorAwidth = *aux;
      else
         mycolorAwidth = mylittleWidth;
	if (!aux) {
		printf ("mycolorAwidth null\n");
  }

   }
	else {
		printf ("aux null\n");
  }

   if ( !(mycolorA && mycolorAresume && mycolorAsuspend && mycolorAheight
       && mycolorAwidth) )
   {
      fprintf(stderr, "followface: not enough variables from varcolorA\n");
      jdeshutdown(1);
   }
 
   aux = (int *)myimport("ptmotors","id");
   if (aux) {
      mysonptmotors=*aux;
      mylongitude=myimport("ptmotors", "longitude");
      mylatitude=myimport ("ptmotors", "latitude");
      mymaxlongitude=myimport("ptmotors", "max_longitude");
      mymaxlatitude=myimport ("ptmotors", "max_latitude");
      myminlongitude=myimport("ptmotors", "min_longitude");
      myminlatitude=myimport ("ptmotors", "min_latitude");
      mymaxlongitude_speed=myimport ("ptmotors", "max_longitude_speed");
      mymaxlatitude_speed=myimport ("ptmotors", "max_latitude_speed");
      mylongitude_speed=myimport("ptmotors", "longitude_speed");
      mylatitude_speed=myimport("ptmotors","latitude_speed");
      ptmotorsresume=(runFn)myimport("ptmotors","run");
      ptmotorssuspend=(stopFn)myimport("ptmotors","stop");
   }
   if ( !(aux && mylongitude && mylatitude && mymaxlongitude && mymaxlatitude
       && myminlongitude && myminlatitude && mymaxlongitude_speed &&
       mymaxlatitude_speed && mylongitude_speed && mylatitude_speed &&
       ptmotorsresume && ptmotorssuspend) )
   {
      fprintf(stderr, "followface: not enough variables from ptmotors");
      jdeshutdown(1);
   }

   aux = (int *)myimport("ptencoders","id");
   if (aux) {
      mysonptencoders=*aux;
      mypan_angle=myimport("ptencoders", "pan_angle");
      mytilt_angle=myimport("ptencoders", "tilt_angle");
      ptencodersresume=(runFn)myimport("ptencoders", "run");
      ptencoderssuspend=(stopFn)myimport("ptencoders", "stop");
   }
   if ( !(aux && mypan_angle && mytilt_angle && ptencodersresume &&
       ptencoderssuspend) )
   {
      fprintf(stderr, "followface: not enough variables from ptencoders");
      jdeshutdown(1);
   }

   aux = (int *)myimport("zmotors","id");
   if (aux) {
      mysonzmotors=*aux;
      myzoom=myimport("zmotors", "zoom");
      myzoom_speed=myimport ("zmotors", "zoom_speed");
      zmotorsresume=(runFn)myimport("zmotors","run");
      zmotorssuspend=(stopFn)myimport("zmotors","stop");
      mymaxzoom=myimport("zmotors", "max_zoom");
      myminzoom=myimport("zmotors", "min_zoom");
      mymaxzoom_speed=myimport ("zmotors", "max_zoom_speed");
   }
//    if ( !(aux && myzoom && myzoom_speed && zmotorsresume && zmotorssuspend
//        && mymaxzoom && myminzoom && mymaxzoom_speed) )
//    {
//       fprintf(stderr, "followface: not enough variables from zmotors");
//       jdeshutdown(1);
//    }

   aux = (int *)myimport("zencoders","id");
   if (aux) {
      mysonzencoders=*aux;
      myzoom_position=myimport("zencoders", "zoom_position");
      zencodersresume=(runFn)myimport("zencoders", "run");
      zencoderssuspend=(stopFn)myimport("zencoders", "stop");
   }
//    if ( !(aux && myzoom_position && zencodersresume && zencoderssuspend) )
//    {
//       fprintf(stderr, "followface: not enough variables from zencoders");
//       jdeshutdown(1);
//    }

   aux = (int *)myimport("haar","id");
   if (aux) {
      mySonHaar=*aux;
      myHaarResume = (runFn)myimport("haar", "run");
      myHaarSuspend = (stopFn)myimport("haar", "stop");
      myHaarFaces = (CvSeq **)myimport("haar", "faces");
      myHaarMutex = (pthread_mutex_t *)myimport("haar", "haarMutex");
      myHaarClock = (unsigned long int *)myimport("haar", "facesClock");
      myHaarDetectionTime = (double *)myimport("haar", "detectionTime");
      myHaarCycle = (int *)myimport("haar", "cycle");
   }
   if ( !(aux && myHaarResume && myHaarSuspend && myHaarFaces && myHaarMutex
       && myHaarClock && myHaarDetectionTime && myHaarCycle) )
   {
      fprintf(stderr, "followface: not enough variables from haar");
      jdeshutdown(1);
   }
}

/*Exportar símbolos*/
void followface_exports(){
   myexport("followface","cycle",&followface_cycle);
   myexport("followface","run",(void *)followface_run);
   myexport("followface","stop",(void *)followface_stop);
   myexport("followface", "img", &imgWorkingA_V->imageData); // al detector de caras
   myexport("followface", "width", &mycolorAwidth);
   myexport("followface", "height", &mycolorAheight);
   myexport("followface", "nChannels", &imgWorkingA_V->nChannels);
}

/*Al suspender el esquema*/
void followface_fin(){
   if (mycolorAsuspend != NULL)
      mycolorAsuspend();
   
   if (ptencoderssuspend != NULL)
      ptencoderssuspend();
   if (ptmotorssuspend != NULL)
      ptmotorssuspend();
   
   if (zencoderssuspend != NULL)
      zencoderssuspend();
   if (zmotorssuspend != NULL)
      zmotorssuspend();

   if (haarDetector) {
      if (myHaarSuspend != NULL)
         myHaarSuspend();
   }
}


void followface_stop()
{
  /* printf("followface: cojo-suspend\n");*/
  pthread_mutex_lock(&(all[followface_id].mymutex));
  put_state(followface_id,slept);
  printf("followface: off\n");
  pthread_mutex_unlock(&(all[followface_id].mymutex));
  /*  printf("followface: suelto-suspend\n");*/
  followface_fin();
}


void followface_run(int father, int *brothers, arbitration fn)
{
  int i;

  /* update the father incorporating this schema as one of its children */
  if (father!=GUIHUMAN && father!=SHELLHUMAN)
    {
      pthread_mutex_lock(&(all[father].mymutex));
      all[father].children[followface_id]=TRUE;
      pthread_mutex_unlock(&(all[father].mymutex));
    }

  pthread_mutex_lock(&(all[followface_id].mymutex));
  /* this schema resumes its execution with no children at all */
  for(i=0;i<MAX_SCHEMAS;i++) all[followface_id].children[i]=FALSE;
  all[followface_id].father=father;
  if (brothers!=NULL)
    {
      for(i=0;i<MAX_SCHEMAS;i++) followface_brothers[i]=-1;
      i=0;
      while(brothers[i]!=-1) {followface_brothers[i]=brothers[i];i++;}
    }
  followface_callforarbitration=fn;
  put_state(followface_id,notready);
  printf("followface: on\n");
  followface_imports();

  if (mycolorAresume != NULL) {
     all[followface_id].children[mysoncolorA]=TRUE;
     mycolorAresume(followface_id, NULL, NULL);
  }
  
  if (ptencodersresume != NULL) {
     all[followface_id].children[mysonptencoders]=TRUE;
     ptencodersresume(followface_id, NULL, NULL);
  }
  if (ptmotorsresume != NULL) {
     all[followface_id].children[mysonptmotors]=TRUE;
     ptmotorsresume(followface_id, NULL, NULL);
  }
  
  if (zencodersresume != NULL) {
     all[followface_id].children[mysonzencoders]=TRUE;
     zencodersresume(followface_id, NULL, NULL);
  }
  if (zmotorsresume != NULL) {
     all[followface_id].children[mysonzmotors]=TRUE;
     zmotorsresume(followface_id, NULL, NULL);
  }
  
   if (haarDetector) {
      if (myHaarResume != NULL) {
         all[followface_id].children[mySonHaar]=TRUE;
         myHaarResume(followface_id, NULL, NULL);
      }
   }

   pthread_cond_signal(&(all[followface_id].condition));
   pthread_mutex_unlock(&(all[followface_id].mymutex));
}

void *followface_thread(void *not_used)
{
   struct timeval a,b;
   long n=0; /* iteration */
   long next,bb,aa;

   for(;;)
   {
      pthread_mutex_lock(&(all[followface_id].mymutex));

      if (all[followface_id].state==slept)
      {
	 followface_state=init;
	 pthread_cond_wait(&(all[followface_id].condition),&(all[followface_id].mymutex));
	 pthread_mutex_unlock(&(all[followface_id].mymutex));
      }
      else
      {
	 /* check preconditions. For now, preconditions are always satisfied*/
	 if (all[followface_id].state==notready)
	    put_state(followface_id,ready);
	 /* check brothers and arbitrate. For now this is the only winner */
	 if (all[followface_id].state==ready)
	 {put_state(followface_id,winner);
	 gettimeofday(&a,NULL);
	 aa=a.tv_sec*1000000+a.tv_usec;
	 n=0;
	 }

	 if (all[followface_id].state==winner)
	    /* I'm the winner and must execute my iteration */
	 {
	    pthread_mutex_unlock(&(all[followface_id].mymutex));
	    /*      gettimeofday(&a,NULL);*/
	    n++;
	    followface_iteration();
	    gettimeofday(&b,NULL);
	    bb=b.tv_sec*1000000+b.tv_usec;
	    next=aa+(n+1)*(long)followface_cycle*1000-bb;
	    if (next>5000)
	    {
	       usleep(next-5000);
	       /* discounts 5ms taken by calling usleep itself, on average */
	    }
	    else  ;
	 }
	 else
	    /* just let this iteration go away. overhead time negligible */
	 {
	    pthread_mutex_unlock(&(all[followface_id].mymutex));
	    usleep(followface_cycle*1000);
	 }
      }
   }
}

void followface_init()
{
  pthread_mutex_lock(&(all[followface_id].mymutex));
  printf("followface schema started up\n");
//   creaImagenes();
//   followface_exports();
  put_state(followface_id,slept);
  pthread_create(&(all[followface_id].mythread),NULL,followface_thread,NULL);
  pthread_mutex_unlock(&(all[followface_id].mymutex));
  //followface_init();
}

void followface_guibuttons(FL_OBJECT *obj){

   if (obj == fd_followfacegui->_joystick) {
      x = fl_get_positioner_xvalue(obj);
      y = fl_get_positioner_yvalue(obj);
      fl_redraw_object(fd_followfacegui->_joystick);
   }
   else if (obj == fd_followfacegui->_zoom) {
      z = fl_get_counter_value(obj);
      if (myzoom && mymaxzoom && myminzoom) {
         *myzoom = *myminzoom + z*(*mymaxzoom - *myminzoom);
      }
   }
   else if (obj == fd_followfacegui->_zoomSpeed) {
      z_speed = fl_get_counter_value(obj);
      if (myzoom_speed && mymaxzoom_speed) {
         *myzoom_speed = z_speed * *mymaxzoom_speed;
      }
   }
   else if (obj == fd_followfacegui->_tiltSpeed) {
      y_speed = fl_get_counter_value(obj);
   }
   else if (obj == fd_followfacegui->_panSpeed) {
      x_speed = fl_get_counter_value(obj);
   }
   else if (obj == fd_followfacegui->hmin)
      skinlikelyhsv[0]=fl_get_counter_value(fd_followfacegui->hmin);
   else if (obj == fd_followfacegui->hmax)
      skinlikelyhsv[1]=fl_get_counter_value(fd_followfacegui->hmax);
   else if (obj == fd_followfacegui->smin)
      skinlikelyhsv[2]=fl_get_counter_value(fd_followfacegui->smin);
   else if (obj == fd_followfacegui->smax)
      skinlikelyhsv[3]=fl_get_counter_value(fd_followfacegui->smax);
   else if (obj == fd_followfacegui->vmin)
      skinlikelyhsv[4]=fl_get_counter_value(fd_followfacegui->vmin);
   else if (obj == fd_followfacegui->vmax)
      skinlikelyhsv[5]=fl_get_counter_value(fd_followfacegui->vmax);
   else if (obj == fd_followfacegui->_haar) {
      if( fl_get_button(obj) ) {
         haarDetector = 1;
         if (myHaarResume != NULL) {
 //           pthread_mutex_lock(&(all[followface_id].mymutex));
            all[followface_id].children[mySonHaar]=TRUE;
            myHaarResume(followface_id, NULL, NULL);
//            pthread_mutex_unlock(&(all[followface_id].mymutex));
         }

      }
      else {
         haarDetector = 0;
         myHaarSuspend();

      }
   }
}

void followface_guidisplay(){

}


void followface_hide(void){
  delete_buttonscallback(followface_guibuttons);
  delete_displaycallback(followface_guidisplay);
  fl_hide_form(fd_followfacegui->followfacegui);
  display_active = 0;
  cvDestroyWindow("followface");
}

void on_mouse( int event, int x, int y, int flags, void* param ) {
   if (event == CV_EVENT_LBUTTONDOWN) {
      targetCentre = cvPoint(x%mycolorAwidth,y%mycolorAheight);
   }
   if (event == CV_EVENT_MBUTTONDOWN) {
      verFiltro = !verFiltro;
   }
}

void followface_show(void){
  static int k=0;

  if (k==0) /* not initialized */
    {
      k++;
      fd_followfacegui = create_form_followfacegui();
      fl_set_form_position(fd_followfacegui->followfacegui,400,50);
      x = fl_get_positioner_xvalue(fd_followfacegui->_joystick);
      y = fl_get_positioner_yvalue(fd_followfacegui->_joystick);
      z = fl_get_counter_value(fd_followfacegui->_zoom);
      x_speed = fl_get_counter_value(fd_followfacegui->_panSpeed);
      y_speed = fl_get_counter_value(fd_followfacegui->_tiltSpeed);
      z_speed = fl_get_counter_value(fd_followfacegui->_zoomSpeed);
      skinlikelyhsv[0] = 170;
      skinlikelyhsv[1] = 30;
      skinlikelyhsv[2] = 71;
      skinlikelyhsv[3] = 145;
      skinlikelyhsv[4] = 100;
      skinlikelyhsv[5] = 256;
//       skinlikelyhsv[0] = 0;
//       skinlikelyhsv[1] = 180;
//       skinlikelyhsv[2] = 0;
//       skinlikelyhsv[3] = 256;
//       skinlikelyhsv[4] = 0;
//       skinlikelyhsv[5] = 256;
      fl_set_counter_value(fd_followfacegui->hmin,skinlikelyhsv[0]);
      fl_set_counter_value(fd_followfacegui->hmax,skinlikelyhsv[1]);
      fl_set_counter_value(fd_followfacegui->smin,skinlikelyhsv[2]);
      fl_set_counter_value(fd_followfacegui->smax,skinlikelyhsv[3]);
      fl_set_counter_value(fd_followfacegui->vmin,skinlikelyhsv[4]);
      fl_set_counter_value(fd_followfacegui->vmax,skinlikelyhsv[5]);
    }
  //register_buttonscallback(followface_guibuttons);
  //register_displaycallback(followface_guidisplay);
  fl_show_form(fd_followfacegui->followfacegui,FL_PLACE_POSITION,FL_FULLBORDER,"followface");
  cvNamedWindow( "followface", 1 );
  cvSetMouseCallback("followface", on_mouse, 0);
  display_active = 1;
}
