/** Header file generated with fdesign on Tue Nov 20 12:36:43 2007.**/

#ifndef FD_mastergui_h_
#define FD_mastergui_h_

/** Callbacks, globals and object handlers **/


/**** Forms and Objects ****/
typedef struct {
	FL_FORM *mastergui;
	void *vdata;
	char *cdata;
	long  ldata;
	FL_OBJECT *exit;
	FL_OBJECT *schemas;
	FL_OBJECT *fps0;
	FL_OBJECT *vis0;
	FL_OBJECT *fps1;
	FL_OBJECT *vis1;
	FL_OBJECT *fps2;
	FL_OBJECT *vis2;
	FL_OBJECT *fps3;
	FL_OBJECT *vis3;
	FL_OBJECT *fps4;
	FL_OBJECT *vis4;
	FL_OBJECT *vis5;
	FL_OBJECT *vis6;
	FL_OBJECT *vis7;
	FL_OBJECT *vis8;
	FL_OBJECT *vis9;
	FL_OBJECT *fps5;
	FL_OBJECT *fps6;
	FL_OBJECT *fps7;
	FL_OBJECT *fps8;
	FL_OBJECT *fps9;
	FL_OBJECT *jdegui;
	FL_OBJECT *guifps;
	FL_OBJECT *fps10;
	FL_OBJECT *vis10;
	FL_OBJECT *fps11;
	FL_OBJECT *vis11;
	FL_OBJECT *hide;
	FL_OBJECT *hierarchy;
	FL_OBJECT *stat0;
	FL_OBJECT *stat1;
	FL_OBJECT *stat2;
	FL_OBJECT *stat3;
	FL_OBJECT *stat4;
	FL_OBJECT *stat5;
	FL_OBJECT *stat6;
	FL_OBJECT *stat7;
	FL_OBJECT *stat8;
	FL_OBJECT *stat9;
	FL_OBJECT *stat10;
	FL_OBJECT *stat11;
	FL_OBJECT *act6;
	FL_OBJECT *act0;
	FL_OBJECT *act1;
	FL_OBJECT *act2;
	FL_OBJECT *act3;
	FL_OBJECT *act5;
	FL_OBJECT *act4;
	FL_OBJECT *act7;
	FL_OBJECT *act8;
	FL_OBJECT *act9;
	FL_OBJECT *act10;
	FL_OBJECT *act11;
	FL_OBJECT *vis12;
	FL_OBJECT *fps12;
	FL_OBJECT *fps13;
	FL_OBJECT *vis13;
	FL_OBJECT *fps14;
	FL_OBJECT *vis14;
	FL_OBJECT *stat12;
	FL_OBJECT *stat13;
	FL_OBJECT *stat14;
	FL_OBJECT *act12;
	FL_OBJECT *act13;
	FL_OBJECT *act14;
	FL_OBJECT *fps15;
	FL_OBJECT *vis15;
	FL_OBJECT *fps16;
	FL_OBJECT *vis16;
	FL_OBJECT *stat15;
	FL_OBJECT *stat16;
	FL_OBJECT *act15;
	FL_OBJECT *act16;
	FL_OBJECT *vis17;
	FL_OBJECT *fps17;
	FL_OBJECT *fps18;
	FL_OBJECT *vis18;
	FL_OBJECT *fps19;
	FL_OBJECT *vis19;
	FL_OBJECT *stat17;
	FL_OBJECT *stat18;
	FL_OBJECT *stat19;
	FL_OBJECT *act17;
	FL_OBJECT *act18;
	FL_OBJECT *act19;
} FD_mastergui;

extern FD_mastergui * create_form_mastergui(void);

#endif /* FD_mastergui_h_ */
