/*
 *  Copyright (C) 2009 Julio M. Vega Pérez 
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio M. Vega Pérez <julio.vega@urjc.es>
 */
#include <GL/gl.h>              
#include <GL/glx.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <GL/freeglut_std.h>
#include <forms.h>
#include <glcanvas.h>

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gtk/gtkgl.h>
#include <gtkextra/gtkextra.h>

#include <jde.h>
#include <pioneer.h>
#include <graphics_gtk.h>
#include <graphics_xforms.h>
#include <cv.h>
#include <highgui.h>

#define MIN_EXP_WIDTH 20
#define MIN_EXP_HEIGHT 20
#define v3f glVertex3f
#define MIN_PIXELES 400          /* Numero minimo de pixeles filtrados */
#define MIN_DISTANCIA 40         /* CTE Minima distancia para mover la PANTILT */
#define CTE_PTU 1.9              /* CTE que nos convierte de distancia a unidades PANTILT */
#define ENCOD_TO_DEG (3.086/60.) /* CTE que nos pasa de unidades PANTILT a grados */
#define DEG_TO_ENCOD (60./3.086) /* CTE que nos pasa de grados a unidades PANTILT */
#define MAXPAN_POS 95
#define MAXPAN_NEG -95
#define VEL_MAX_PAN 2500.0*ENCOD_TO_DEG
#define VEL_MIN_PAN 350.0*ENCOD_TO_DEG
#define POS_MIN_PAN 40.0
#define POS_MAX_PAN 160.0
#define VEL_MAX_TILT 1000.0*ENCOD_TO_DEG
#define VEL_MIN_TILT 300.0*ENCOD_TO_DEG
#define POS_MIN_TILT 40.0
#define POS_MAX_TILT 120.0

/* directed perception pantilt limits */
#define MAX_PAN_ANGLE 158. /* degrees */
#define MIN_PAN_ANGLE -158. /* degrees */
#define MAX_TILT_ANGLE 30. /* degrees */
#define MIN_TILT_ANGLE -46. /* degrees */
#define MAX_SPEED_PANTILT 205.89

#define RADIO_MAX 258
#define RADIO_MIN 0
#define ANCHO_ESCENA_COMPUESTA 520
#define ALTO_ESCENA_COMPUESTA 520
#define CENTRO_X 260
#define CENTRO_Y 260
#define PI 3.14159265
#define BLANCO 0x000000FF
#define GRIS 0x000000C2
#define NEGRO 0x00000000
#define ANCHO_CAPTURA 46.64//normal
#define ALTO_CAPTURA 36.88//normal
#define ESCENA_PAN_MAX (MAX_PAN_ANGLE + (ANCHO_CAPTURA / 2.))
#define ESCENA_PAN_MIN (MIN_PAN_ANGLE - (ANCHO_CAPTURA / 2.))
#define ESCENA_TILT_MAX (MAX_TILT_ANGLE + (ALTO_CAPTURA/ 2.))
#define ESCENA_TILT_MIN (MIN_TILT_ANGLE - (ALTO_CAPTURA / 2.))
#define TILT_MAX 90
#define TILT_MIN -90
#define MAXWORLD 20000.
#define TIME_TO_CHANGE_SCENE 8
#define TIME_TO_REFRESH_FACES 3

#define SALIENCY_INCREMENT 1
#define SALIENCY_DECREMENT 1
#define MAX_SALIENCY 20
#define MIN_SALIENCY 0

#define LIVELINESS_INCREMENT 20
#define LIVELINESS_DECREMENT 10
#define MIN_LIVELINESS 0
#define MAX_LIVELINESS 5000
#define LIVELINESS_TO_DEAD -1
#define PENALTY_FACTOR 200
#define BONUS_FACTOR 100

#define FOLLOW_TIME 3
#define TIME_TO_FORCED_SEARCH 5
#define TIME_TO_DEAD 50

struct dfilter {
  int x;                /* Coordenada X del centro de masas */
  int y;                /* Coordenada Y del centro de masas */
  int pixeles;          /* Numero de pixeles que pasan el filtro */
  int cuadrante;        /* Cuadrante en el que se encuentra el centro de masas */
  int distancia;        /* Distancia del centro de la imagen al centro de masas */
  int lineas;           /* Numero de lineas horizontales en las que al menos
                          un pixel pasa el filtro */
};

int longitudeScenePositions[8] = {-159, -119, -79, -39, 39, 79, 119, 159};
int latitudeScenePositions[4] = {31, 14, -14, -31};

typedef struct SoRtype{
  struct SoRtype *father;
  float posx;
  float posy;
  float posz;
  float foax;
  float foay;
  float foaz;
  float roll;
} SofReference;

struct faceStruct {
	double lastInstant; // último instante de tiempo en su detección
	double firstInstant; // primer instante de tiempo en su detección
	int pixel_x;
	int pixel_y;
	float latitude; // posición absoluta del pantilt, en eje tilt
	float longitude; // posición absoluta del pantilt, en eje pan
	float scenePoint_pan;
	float scenePoint_tilt;
	int scenePos; // it can be left, center or right, depends on pantilt pos where face was detected
	struct faceStruct* next;
	float saliency;
	float liveliness;
	int type; // 0 = cara real; 1 = cara virtual
	int isVisited;
};

typedef struct {
	int x;
	int y;
} imagePoint;

typedef struct {
  float x;
  float y;
} imagePoint_float;

typedef struct {
	float pan;
	float tilt;
} scenePoint;

typedef struct {
  int x;
  int y;
} t_vector;

typedef struct {
  float x;
  float y;
} t_vector_float;

enum movement_pantilt {up,down,left,right};
enum state {think, search, analizeSearch, track, analizeTrack};

extern void iFollowFace_init();
extern void iFollowFace_stop();
extern void iFollowFace_run(int father, int *brothers, arbitration fn);
extern void iFollowFace_show();
extern void iFollowFace_hide();

extern int iFollowFace_id; /* schema identifier */
extern int iFollowFace_cycle; /* ms */
